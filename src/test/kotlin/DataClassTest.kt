import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DataClassTest {

    data class Person(
        val firstName: String,
        val lastName: String
    ) {
//    override fun hashCode(): Int {
//        return super.hashCode()
//    }
//    override fun equals(other: Any?): Boolean {
//        println("People compared!")
//        return super.equals(other)
//    }
    }

    @Test
    fun testEquals() {
        val store = mutableMapOf<Person, String>()

        val person1 = Person("Vitaly", "Kravchenko")
        val person2 = Person("Vitaly", "Kravchenko")

        fun getOrPut(person: Person) = store.getOrPut(person, { person.firstName + " " + person.lastName })

        getOrPut(person1)
        assertEquals(store.size, 1, "Store should have one Person.")

        // Contents of person1 and person2 are compared using the data class' 'equals' method
        // and are found to be the same thing, even though they are different objects.
        getOrPut(person2)
        assertEquals(store.size, 1, "Store should still have one Person.")
    }
}