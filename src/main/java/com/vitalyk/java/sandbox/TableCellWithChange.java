package com.vitalyk.java.sandbox;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.Random;
import java.util.function.Function;

public class TableCellWithChange extends Application {

    public static class ChangeAwareCell<S,T> extends TableCell<S,T> {

        public ChangeAwareCell() {
            itemProperty().addListener((obs, oldItem, newItem) -> {
                System.out.printf("In listener, value for %s changed from %s to %s%n",
                        getTableRow().getItem(), oldItem, newItem);
            });
        }

        @Override
        protected void updateItem(T item, boolean empty) {
            T oldItem = getItem();
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
            } else {
                setText(item.toString());
                System.out.printf("Change in %s from %s to %s %n",
                        getTableView().getItems().get(getIndex()), oldItem, item);
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {

        TableView<Item> table = new TableView<>();
        TableColumn<Item, String> itemCol = column("Item", Item::nameProperty);
        table.getColumns().add(itemCol);

        TableColumn<Item, Number> valueCol = column("Value", Item:: valueProperty);
        table.getColumns().add(valueCol);

        valueCol.setCellFactory(tc -> new ChangeAwareCell<>());

        TableColumn<Item, Void> changeCol = new TableColumn<>();
        changeCol.setCellFactory(tc -> new TableCell<Item, Void>() {
            private Button incButton = new Button("^");
            private Button decButton = new Button("v");
            private HBox graphic = new HBox(2, incButton, decButton);
            {
                incButton.setOnAction(e -> {
                    Item item = (Item) getTableRow().getItem();
                    item.setValue(item.getValue()+1);
                });
                decButton.setOnAction(e -> {
                    Item item = (Item) getTableRow().getItem();
                    item.setValue(item.getValue()-1);
                });
            }
            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(graphic);
                }
            }
        });
        table.getColumns().add(changeCol);

        Random rng = new Random();
        for (int i = 1 ; i <= 20  ; i++) {
            table.getItems().add(new Item("Item "+i, rng.nextInt(100)));
        }

        Scene scene = new Scene(table);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private <S,T> TableColumn<S,T> column(String text, Function<S, ObservableValue<T>> property) {
        TableColumn<S,T> col = new TableColumn<>(text);
        col.setCellValueFactory(cellData -> property.apply(cellData.getValue()));
        col.setPrefWidth(150);
        return col ;
    }


    public static class Item {
        private final StringProperty name = new SimpleStringProperty();
        private final IntegerProperty value = new SimpleIntegerProperty();

        public Item(String name, int value) {
            setName(name);
            setValue(value);
        }

        @Override
        public String toString() {
            return getName();
        }

        public final StringProperty nameProperty() {
            return this.name;
        }


        public final String getName() {
            return this.nameProperty().get();
        }


        public final void setName(final String name) {
            this.nameProperty().set(name);
        }


        public final IntegerProperty valueProperty() {
            return this.value;
        }


        public final int getValue() {
            return this.valueProperty().get();
        }


        public final void setValue(final int value) {
            this.valueProperty().set(value);
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}