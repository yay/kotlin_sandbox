package com.vitalyk.kotlin.sandbox

import javafx.application.Application
import javafx.geometry.VPos
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.shape.Circle
import javafx.scene.shape.Line
import javafx.scene.shape.Shape
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.scene.text.Text
import tornadofx.App
import tornadofx.View

/**
 * Binary Tree - a tree where nodes can have at most two children.
 *
 *              G <- grandparent
 *             / \
 *   uncle -> U  P <- parent
 *              /
 *             N
 *
 * a 2-node has one data item, and if internal has two child nodes;
 * a 3-node has two data items, and if internal has three child nodes;
 * a 4-node has three data items, and if internal has four child nodes.
 *
 * A 2-3 tree is a tree composed of 2- and 3-nodes. A 2-3 tree is a B-tree of order 3.
 * A 2–3–4 (or 2-4) tree is a tree composed of 2-, 3- and 4-nodes. A 2-4 tree is B-tree of order 4.
 *
 * Binary Search Tree - a binary tree where for each node values of all the nodes
 * in the left subtree are smaller, and values of all the nodes in the right
 * subtree are larger. BST is an ordered set (duplicates are not allowed).
 *
 * Balanced Search Tree - a BST with a guaranteed height of O(log n) for n items.
 *
 * Red-Black tree properties:
 * - a node is either red or black
 * - the root node and NULL nodes are black
 * - if a node is red, then its children are black
 * - every path from the root to a NULL node has the same number of black nodes
 *   (called the black-height of the node; the node itself doesn't count)
 * - the longest path (root to farthest NULL) is no more than twice the length
 *   of the shortest path (root to nearest NULL):
 *   - shortest path: all black nodes
 *   - longest path: alternating red and black nodes
 * - time complexity for search, insert and remove is O(log n)
 *
 * Side node: the AVL tree is another structure supporting O(log n) search, insertion, removal.
 * It is more rigidly balanced than red–black trees, leading to slower insertion and removal
 * but faster retrieval. This makes it attractive for data structures that may be built once
 * and loaded without reconstruction.
 *
 * Rotations alter the structure of a tree (on insertion or deletion of a node,
 * which may result in violation of red-black tree properties) by rearranging
 * subtrees with the intent of decreasing the height of the tree. Larger subtrees
 * are moved up and smaller subtrees are moved down. This does not affect the order
 * of items: smaller ones will still be to the left and larger ones to the right.
 *
 * Two types of rotations: left and right.
 *
 *        Left-rotate (on 5)               Right-rotate (on 10)
 *
 *        5              10                  10              5
 *      /  \            /  \                /  \           /  \
 *     2   10          5   12              5   12         2   10
 *        / \   -->   / \                 / \      -->       / \
 *      8   12       2   8               2   8             8   12
 *    /  \             /  \                /  \          /  \
 *   6   9            6   9               6   9         6   9
 *
 *
 * Rotations have a time complexity of O(1).
 *
 *
 * One way to view red-black BST algorithms is as maintaining the following invariant
 * properties under insertion and deletion:
 * - no path from the root to a null node contains two consecutive red nodes
 * - the number of black nodes on every such path is the same
 *
 */

fun pickNode(node: Node, sceneX: Double, sceneY: Double): Node? {

    if (node.contains(node.sceneToLocal(sceneX, sceneY, true))) {
        return node
    } else if (node is Parent) {
        node.childrenUnmodifiable.asReversed().forEach { child ->
            val p = child.sceneToLocal(sceneX, sceneY, true)
            if (child.isVisible && !child.isMouseTransparent && child.contains(p)) {
                return child
            }
        }
    }
    return null
}

typealias NodeHandler<K, V> = (key: K, value: V?) -> Unit

/**
 * Left-leaning Red-Black Tree implementation.
 */
class LLRB<K: Comparable<K>, V> {

    companion object {
        @JvmStatic val RED   = true
        @JvmStatic val BLACK = false
    }

    /* private */ inner class Node (
        var key: K,
        var value: V?
    ) {
        var left: Node? = null
        var right: Node? = null
        var parent: Node? = null
        var color: Boolean = RED
    }

    /* private */ var root: Node? = null

    fun search(key: K): V? {
        var x = root

        while (x != null) {
            val cmp = key.compareTo(x.key)
            when {
                cmp == 0 -> return x.value
                cmp < 0 -> x = x.left
                cmp > 0 -> x = x.right
            }
        }

        return null
    }

    fun insert(key: K, value: V?) {
        val node = insert(root, key, value)
        node.color = BLACK
        root = node
    }

    fun insert(vararg values: Pair<K, V?>) {
        values.forEach { (first, second) -> insert(first, second) }
    }

    fun insert(vararg values: K) {
        values.forEach { value -> insert(value, null) }
    }

    private fun insert(node: Node?, key: K, value: V?): Node {
        if (node == null) return Node(key, value)

        if (isRed(node.left) && isRed(node.right)) flipColors(node)

        val cmp = key.compareTo(node.key)
        when {
            cmp == 0 -> node.value = value
            cmp < 0  -> node.left = insert(node.left, key, value)
            cmp > 0  -> node.right = insert(node.right, key, value)
        }

        val result: Node
        when {
            isRed(node.right) && !isRed(node.left)     -> result = rotateLeft(node)
            isRed(node.left) && isRed(node.left?.left) -> result = rotateRight(node)
            else                                       -> result = node
        }

        return result
    }

//    fun deleteMin() {
//        root = deleteMin(root)
//        root.color = BLACK
//    }

//    private fun deleteMin(node: Node): Node? {
//        var u = node
//        val left = u.left ?: return null
//
//        if (!isRed(left) && !isRed(left.left))
//            u = moveRedLeft(u)
//
//        u.left = deleteMin(u.left)
//
//        return fixUp(u)
//    }

//    private fun moveRedLeft(node: Node): Node {
//        var u = node
//        flipColors(u)
//        if (isRed(u.right.left)) {
//            u.right = rotateRight(u.right)
//            u = rotateLeft(u)
//            flipColors(u)
//        }
//        return u
//    }
//
//    private fun moveRedRight(node: Node): Node {
//        var u = node
//        flipColors(u)
//        if (isRed(u.left.left)) {
//            u = rotateRight(u)
//            flipColors(u)
//        }
//        return u
//    }
//
//    fun delete(key: K) {
//        root = delete(root, key)
//        root.color = BLACK
//    }

//    private fun delete(node: Node, key: K): Node {
//        var u = node
//        if (key.compareTo(u.key) < 0) {
//            if (!isRed(u.left) && !isRed(u.left.left))
//                u = moveRedLeft(u)
//            u.left = delete(u.left, key)
//        } else {
//            if (isRed(u.left))
//                u = rotateRight(u)
//            if (key.compareTo(u.key) == 0 && u.right == null)
//                return null
//            if (!isRed(u.right) && !isRed(u.right.left))
//                u = moveRedRight(u)
//            if (key.compareTo(u.key) == 0) {
//                u.value = get(u.right, min(u.right).key)
//                u.key = min(u.right).key
//                u.right = deleteMin(u.right)
//            } else
//                u.right = delete(u.right, key)
//        }
//        return fixUp(u)
//    }

    private fun isRed(node: Node?) = node != null && node.color == RED

    /**
     * Node 'a' can be left or right, red or black.
     *
     *     a                b
     *    / \              / \
     *   *   b    -->     a   *
     *      / \          / \
     *     *  *         *  *
     */
    private fun rotateLeft(a: Node): Node {
        val b = a.right!!
        a.right = b.left
        b.left = a
        b.color = a.color
        a.color = RED
        return b
    }

    /**
     *       b             a
     *      / \           / \
     *     a   *   -->   *   b
     *    / \               / \
     *   *  *              *  *
     */
    private fun rotateRight(b: Node): Node {
        val a = b.left!!
        b.left = a.right
        a.right = b
        a.color = b.color
        b.color = RED
        return a
    }

    /**
     *       u
     *     /   \
     *    *    *
     *   / \  / \
     */
    private fun flipColors(x: Node) {
        x.color = !x.color
        x.left!!.color = !x.left!!.color
        x.right!!.color = !x.right!!.color
    }

    /**
     * When to use Pre-Order, In-order or Post-Order?
     *
     * Pick the strategy that brings you the nodes you require the fastest.
     *
     * 1. If you know you need to explore the roots before inspecting any leaves, you pick pre-order
     * because you will encounter all the roots before all of the leaves.
     *
     * 2. If you know you need to explore all the leaves before any nodes, you select post-order
     * because you don't waste any time inspecting roots in search for leaves.
     *
     * 3. If you know that the tree has an inherent sequence in the nodes, and you want to flatten
     * the tree back into its original sequence, than an in-order traversal should be used.
     * The tree would be flattened in the same way it was created. A pre-order or post-order
     * traversal might not unwind the tree back into the sequence which was used to create it.
     */

    /**
     * 1) visit the root
     * 2) traverse the left subtree
     * 3) traverse the right subtree
     */
    fun preOrder(fn: NodeHandler<K, V>) {
        preOrder(fn, root)
    }

    private fun preOrder(fn: NodeHandler<K, V>, root: Node?) {
        if (root != null) {
            fn(root.key, root.value)
            preOrder(fn, root.left)
            preOrder(fn, root.right)
        }
    }

    /**
     * 1) traverse the left subtree
     * 2) visit the root
     * 3) traverse the right subtree
     */
    fun inOrder(fn: NodeHandler<K, V>) {
        inOrder(fn, root)
    }

    private fun inOrder(fn: NodeHandler<K, V>, root: Node?) {
        if (root != null) {
            inOrder(fn, root.left)
            fn(root.key, root.value)
            inOrder(fn, root.right)
        }
    }

    /**
     * 1) traverse the left subtree
     * 2) traverse the right subtree
     * 3) visit the root
     */
    fun postOrder(fn: NodeHandler<K, V>) {
        postOrder(fn, root)
    }

    private fun postOrder(fn: NodeHandler<K, V>, root: Node?) {
        if (root != null) {
            postOrder(fn, root.left)
            postOrder(fn, root.right)
            fn(root.key, root.value)
        }
    }

    fun isBST(): Boolean = isBST(root)

    private fun isBST(node: Node?, l: K? = null, r: K? = null): Boolean =
        (node == null) ||
            (l == null || l < node.key) &&
            (r == null || r > node.key) &&
            isBST(node.left, l, node.key) &&
            isBST(node.right, node.key, r)

    /**
     * Note: another way of checking if a BT is a BST is to see if the items go
     * in ascending order during in-order traversal, by comparing the last visited
     * node with the current one.
     */
}

class BstView : View() {
    override val root = Pane()

    private val width = 800.0
    private val height = 800.0

    init {
        primaryStage.minWidth = width
        primaryStage.minHeight = height

        var previousNode: Node? = null
        var previousFill: Paint? = null
        root.setOnMouseMoved { event ->
            val node = pickNode(event.target as Node, event.sceneX, event.sceneY)

            if (node is Shape) {
                if (previousNode != null && previousFill != null) {
                    (previousNode as Shape).fill = previousFill
                }
                previousNode = node
                previousFill = node.fill

                node.fill = Color.RED
            } else {
                if (previousNode != null && previousFill != null) {
                    (previousNode as Shape).fill = previousFill
                }
            }
        }

        // Each node has its own group, which contains that node and all of its descendants.
        fun <K: Comparable<K>, V> renderTree(node: LLRB<K, V>.Node?, group: Group, x: Double, y: Double, depth: Int = 0) {
            if (node == null) {
                return
            }

            val g = Group()
            group.add(g)

            var nx: Double
            var ny: Double

            val d = 150.0 / (depth + 1)
            val r = 20.0


            val left = node.left
            val right = node.right

            if (left != null) {
                nx = x - d
                ny = y + d
                g.add(Line(x, y, nx, ny))
                renderTree(left, g, nx, ny, depth + 1)
            }

            if (right != null) {
                nx = x + d
                ny = y + d
                g.add(Line(x, y, nx, ny))
                renderTree(right, g, nx, ny, depth + 1)
            }

            val circle = Circle(x, y, r)
            circle.fill = Color.YELLOW
            g.add(circle)

            val text = Text(node.key.toString())
            text.font = Font.font("Tahoma", FontWeight.BOLD, 14.0)
            text.textOrigin = VPos.CENTER
            text.x = x - text.layoutBounds.width / 2
            text.y = y
            g.add(text)
        }

        val group = Group()
        root.add(group)

        val tree = LLRB<Int, Any>()

        tree.insert(1, 2, 3, 4, 5, 6, 7, 8, 9)

        renderTree(tree.root, group, width / 2, 50.0)
    }
}

class BstApp : App(BstView::class)

fun main(args: Array<String>) {
    Application.launch(BstApp::class.java, *args)

//    val tree = BST(7, 1, 0, 3, 2, 5, 4, 6, 9, 8, 10)
//    println("\npre-order")
//    tree.preOrder { (key) -> print(key); print(" ") }
//    println("\nin-order")
//    tree.inOrder { (key) -> print(key); print(" ") }
//    println("\npost-order")
//    tree.postOrder { (key) -> print(key); print(" ") }

//    val root = RedBlackNode(10, null, RedBlackNode(15, RedBlackNode(5)))
//    println(isBST(root))
}