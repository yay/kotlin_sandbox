/**
 * The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * based on Jarkko Miettinen's Java program
 * contributed by Tristan Dupont
 * *reset*
 */

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

object BinaryTrees {

    private val MIN_DEPTH = 4
    private val EXECUTOR_SERVICE = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())

    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        var n = 0
        if (args.isNotEmpty()) {
            n = args[0].toInt()
        }

        val maxDepth = if (n < MIN_DEPTH + 2) MIN_DEPTH + 2 else n
        val stretchDepth = maxDepth + 1

        println("stretch tree of depth " + stretchDepth + "\t check: "
            + bottomUpTree(stretchDepth).check())

        val longLivedTree = bottomUpTree(maxDepth)

        val results = arrayOfNulls<String>((maxDepth - MIN_DEPTH) / 2 + 1)

        var d = MIN_DEPTH
        while (d <= maxDepth) {
            val depth = d
            EXECUTOR_SERVICE.execute {
                var check = 0

                val iterations = 1 shl maxDepth - depth + MIN_DEPTH
                for (i in 1..iterations) {
                    val treeNode1 = bottomUpTree(depth)
                    check += treeNode1.check()
                }
                results[(depth - MIN_DEPTH) / 2] =
                    iterations.toString() + "\t trees of depth " + depth + "\t check: " + check
            }
            d += 2
        }

        EXECUTOR_SERVICE.shutdown()
        EXECUTOR_SERVICE.awaitTermination(120L, TimeUnit.SECONDS)

        for (str in results) {
            println(str)
        }

        println("long lived tree of depth " + maxDepth +
            "\t check: " + longLivedTree.check())

        val runtime = Runtime.getRuntime()
        val megabyte = 1024 * 1024
        val totalMemory = runtime.totalMemory()
        val usedMemory = totalMemory - runtime.freeMemory()
        println("Used memory: ${usedMemory / megabyte}MB")
        println("Total memory: ${totalMemory / megabyte}MB")
    }

    private fun bottomUpTree(depth: Int): TreeNode {
        return if (depth > 0) {
            TreeNode(bottomUpTree(depth - 1), bottomUpTree(depth - 1))
        } else TreeNode()
    }

    data class TreeNode(
        private val left: TreeNode? = null,
        private val right: TreeNode? = null
    ) {
        fun check(): Int = if (left == null) 1 else 1 + left.check() + right!!.check()
    }

}