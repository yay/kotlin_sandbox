package com.vitalyk.kotlin.sandbox

// http://forum.devmaster.net/t/fast-and-accurate-sine-cosine/9648
// http://stackoverflow.com/questions/2284860/how-does-c-compute-sin-and-other-math-functions
//

object SinCos {
    fun sin(rad: Float): Float = sin[(rad * radToIndex).toInt() and SIN_MASK]
    fun cos(rad: Float): Float = cos[(rad * radToIndex).toInt() and SIN_MASK]

    fun sin(x: Double): Double = this.sin(x.toFloat()).toDouble()
    fun cos(x: Double): Double = this.cos(x.toFloat()).toDouble()

    fun sinDeg(deg: Float): Float = sin[(deg * degToIndex).toInt() and SIN_MASK]
    fun cosDeg(deg: Float): Float = cos[(deg * degToIndex).toInt() and SIN_MASK]

    private val RAD: Float = Math.PI.toFloat() / 180.0f
    private val DEG: Float = 180.0f / Math.PI.toFloat()
    private val SIN_BITS: Int = 12
    private val SIN_MASK: Int = (-1 shl SIN_BITS).inv()
    private val SIN_COUNT: Int = SIN_MASK + 1
    private val radFull: Float = (Math.PI * 2.0).toFloat()
    private val degFull: Float = 360.0.toFloat()
    private val radToIndex: Float = SIN_COUNT / radFull
    private val degToIndex: Float = SIN_COUNT / degFull
    private val sin: FloatArray = FloatArray(SIN_COUNT)
    private val cos: FloatArray = FloatArray(SIN_COUNT)

    init {
        for (i in 0 until SIN_COUNT) {
            val v = ((i + 0.5f) / SIN_COUNT * radFull).toDouble()
            sin[i] = Math.sin(v).toFloat()
            cos[i] = Math.cos(v).toFloat()
        }

        // Four cardinal directions (credits: Nate)
        var i = 0
        while (i < 360) {
            val idx = (i * degToIndex).toInt() and SIN_MASK
            val v = i * Math.PI / 180.0
            sin[idx] = Math.sin(v).toFloat()
            cos[idx] = Math.cos(v).toFloat()
            i += 90
        }
    }
}
