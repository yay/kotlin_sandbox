package com.vitalyk.kotlin.sandbox

import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.image.PixelFormat
import javafx.scene.layout.Pane
import javafx.stage.Stage
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Callable
import java.util.concurrent.ForkJoinPool

// GLSL fragment shaders in JavaScript
// Reference: https://licson.net/post/glsl-fragment-shaders-in-javascript/

class ShaderApp: Application() {

    override fun start(primaryStage: Stage) {
        val side = 800.0
        val canvas = Canvas(side, side)
        val pane = Pane()
        val scene = Scene(pane, side, side)

        primaryStage.scene = scene
        primaryStage.title = "Shader"
        primaryStage.show()

        pane.children.add(canvas)

        FloatShaderTimer(canvas) { fps ->
            primaryStage.title = "FPS: $fps"
        }.start()
    }
}

private inline fun IntArray.fillWithThreads(width: Int, height: Int, crossinline init: (Int, Int) -> Int) {
    val chunks = 4
    val bandwidth = Math.floor(width.toDouble() / chunks).toInt()

    ForkJoinPool.commonPool().let {
        (0 until chunks).map { i ->
            it.submit( Callable {
                val x1 = i * bandwidth
                val x2 = Math.min(i * bandwidth + bandwidth, width)

                for (x in x1 until x2) {
                    for (y in 0 until height) {
                        this[y * height + x] = init(x, y)
                    }
                }
            } )
        }.forEach { it.get() }
    }
}

private inline fun IntArray.fillWithCoroutines(width: Int, height: Int, crossinline init: (Int, Int) -> Int) {
    val chunks = 4
    val bandwidth = Math.floor(width.toDouble() / chunks).toInt()

    runBlocking {
        (0 until chunks).map { i ->
            GlobalScope.launch {
                val x1 = i * bandwidth
                val x2 = Math.min(i * bandwidth + bandwidth, width)

                for (x in x1 until x2) {
                    for (y in 0 until height) {
                        this@fillWithCoroutines[y * height + x] = init(x, y)
                    }
                }
            }
        }.forEach { it.join() }
    }
}

const val strictMath = false // StrictMath is slow because of JNI overhead
// http://royvanrijn.com/blog/2012/07/java-speed-of-math/

// Results (single thread) on Intel Core i7-4850HQ with 800x800 canvas:
// 1-3 FPS with StrictMath (JS version is a few times faster)
// 25-28 FPS with SinCos (lookup tables)

fun sin(x: Double): Double = if (strictMath) StrictMath.sin(x) else SinCos.sin(x)
fun cos(x: Double): Double = if (strictMath) StrictMath.cos(x) else SinCos.cos(x)

fun sin(x: Float): Float = SinCos.sin(x)
fun cos(x: Float): Float = SinCos.cos(x)

class FloatShaderTimer(canvas: Canvas, private val fps: (Int) -> Unit) : AnimationTimer() {
    private val ctx = canvas.graphicsContext2D
    private val pw = ctx.pixelWriter

    private val w = canvas.width.toInt()
    private val h = canvas.height.toInt()
    private val intPixelFormat = PixelFormat.getIntArgbPreInstance()
    private val intPixels = IntArray(w * h)

    private val start = System.nanoTime()
    private var frameCounter = 0
    private var fpsCounter = 0
    private var secondCounter: Long = 0

    override fun handle(now: Long) {
        val deltaSeconds: Long = (now - start) / 1_000_000_000
        val t: Float = (now - start).toFloat() / 1_000_000_000 * 0.2f

        intPixels.fillWithCoroutines(w, h) { i, j ->
            var x = i.toFloat()
            var y = j.toFloat()

            val u1 = x / w * 2f - 1f
            val u2 = y / w * 2f - h / w

            val tt = sin(t / 8f) * 64f
            x = u1 * tt + sin(t * 2.1f) * 4f
            y = u2 * tt + cos(t * 2.3f) * 4f
            var c = sin(x) + sin(y)
            val zoom = sin(t)
            x = x * zoom * 2f + sin(t * 1.1f)
            y = y * zoom * 2f + cos(t * 1.3f)
            val xx = cos(t * 0.7f) * x - sin(t * 0.7f) * y
            val yy = sin(t * 0.7f) * x + cos(t * 0.7f) * y
            c = (sin(c + sin(xx) + sin(yy)) + 1f) * 0.4f
            val v = 2f - Math.sqrt((u1 * u1 + u2 * u2).toDouble()).toFloat() * 2f

            var r = v * (c + v * 0.4f)
            var g = v * (c * c - 0.5f + v * 0.5f)
            var b = v * (c * 1.9f)

            if (r < 0) {
                r = 0f
            } else if (r > 1) {
                r = 1f
            }
            if (g < 0) {
                g = 0f
            } else if (g > 1) {
                g = 1f
            }
            if (b < 0) {
                b = 0f
            } else if (b > 1) {
                b = 1f
            }

            val ir = (r * 255.0).toInt()
            val ig = (g * 255.0).toInt()
            val ib = (b * 255.0).toInt()

            (255 shl 24) or (ir shl 16) or (ig shl 8) or ib
        }

        pw.setPixels(0, 0, w, h, intPixelFormat, intPixels, 0, w)


        fpsCounter++
        if (deltaSeconds > secondCounter) {
            fps(fpsCounter)
            fpsCounter = 0
        }
        secondCounter = deltaSeconds

        frameCounter++
    }
}

class DoubleShaderTimer(canvas: Canvas, private val fps: (Int) -> Unit) : AnimationTimer() {
    private val ctx = canvas.graphicsContext2D
    private val pw = ctx.pixelWriter

    private val w = canvas.width.toInt()
    private val h = canvas.height.toInt()
    private val intPixelFormat = PixelFormat.getIntArgbPreInstance()
    private val intPixels = IntArray(w * h)

    private val start = System.nanoTime()
    private var frameCounter = 0
    private var fpsCounter = 0
    private var secondCounter: Long = 0

    override fun handle(now: Long) {
        val deltaSeconds: Long = (now - start) / 1_000_000_000
        val t: Double = (now - start).toDouble() / 1_000_000_000 * 0.2

        intPixels.fillWithCoroutines(w, h) { i, j ->
            var x = i.toDouble()
            var y = j.toDouble()

            val u1 = x / w * 2 - 1
            val u2 = y / w * 2 - h / w

            val tt = sin(t / 8.0) * 64
            x = u1 * tt + sin(t * 2.1) * 4.0
            y = u2 * tt + cos(t * 2.3) * 4.0
            var c = sin(x) + sin(y)
            val zoom = sin(t)
            x = x * zoom * 2.0 + sin(t * 1.1)
            y = y * zoom * 2.0 + cos(t * 1.3)
            val xx = cos(t * 0.7) * x - sin(t * 0.7) * y
            val yy = sin(t * 0.7) * x + cos(t * 0.7) * y
            c = (sin(c + (sin(xx) + sin(yy))) + 1.0) * 0.4
            val v = 2 - Math.sqrt(u1 * u1 + u2 * u2) * 2

            var r = v * (c + v * 0.4)
            var g = v * (c * c - 0.5 + v * 0.5)
            var b = v * (c * 1.9)

            if (r < 0) {
                r = 0.0
            } else if (r > 1) {
                r = 1.0
            }
            if (g < 0) {
                g = 0.0
            } else if (g > 1) {
                g = 1.0
            }
            if (b < 0) {
                b = 0.0
            } else if (b > 1) {
                b = 1.0
            }

            val ir = (r * 255.0).toInt()
            val ig = (g * 255.0).toInt()
            val ib = (b * 255.0).toInt()

            (255 shl 24) or (ir shl 16) or (ig shl 8) or ib
        }

        pw.setPixels(0, 0, w, h, intPixelFormat, intPixels, 0, w)


        fpsCounter++
        if (deltaSeconds > secondCounter) {
            fps(fpsCounter)
            fpsCounter = 0
        }
        secondCounter = deltaSeconds

        frameCounter++
    }
}

fun main(args: Array<String>) {
    Application.launch(ShaderApp::class.java, *args)
}