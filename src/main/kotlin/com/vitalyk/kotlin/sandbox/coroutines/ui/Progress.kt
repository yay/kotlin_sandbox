package com.vitalyk.kotlin.sandbox.coroutines.ui

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.atomic.AtomicInteger

fun main(args: Array<String>) {
    runBlocking {
        val range = (1..1000)

        // Wrong
        var n = 0
        range.map {
            launch {
                ++n
            }
        }.map { it.join() }
        println("$n ${range.count()}")

        // Bad
        var i = 0
        range.map {
            launch(Unconfined) {
                ++i
            }
        }.map { it.join() }
        println("$i ${range.count()}")

        // Better
        val j = AtomicInteger()
        range.map {
            launch {
                j.incrementAndGet()
            }
        }.map { it.join() }
        println("${j.get()} ${range.count()}")

        // https://stackoverflow.com/questions/51105840/kotlin-coroutines-progress-counter/51106878#51106878
        // https://kotlinlang.org/docs/tutorials/coroutines-basic-jvm.html
        // Best
        val progressActor = GlobalScope.actor<Int> {
            val count = range.count()
            var k = 0
            for (progress in channel) println("${++k} $count")
        }
        range.map {
            launch(Dispatchers.JavaFx) {
                progressActor.offer(1)
            }
        }.map { it.join() }
    }
}