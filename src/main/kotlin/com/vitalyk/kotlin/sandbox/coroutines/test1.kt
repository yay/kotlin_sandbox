package com.vitalyk.kotlin.sandbox.coroutines

import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx

fun main(args: Array<String>) {
    // If no context is provided (default one is used), the program will exit immediately,
    // without ever printing "Hello".
    // If JavaFx context is used, the program will _never_ exit and keep printing
    // "Hello" indefinitely.
    GlobalScope.launch(Dispatchers.JavaFx) {
        while (isActive) {
            println("Hello")
            delay(1000 * 10)
        }
    }
}