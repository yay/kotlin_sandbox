package com.vitalyk.kotlin.sandbox.coroutines

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.first
import java.io.File
import java.io.IOException
import kotlin.concurrent.thread

// Coroutines by Roman Elizarov:
// https://www.youtube.com/watch?v=_hfBv0a09Jc
// https://www.youtube.com/watch?v=YrrUCSi72E8

/*

The problem with async.

Both are valid invocations in C#:

Concurrent behavior (default):
requestToken()        VALID -> produces Task<Token>  (where Task is a future)

Sequential behavior:
await requestToken()  VALID -> produces Token

Kotlin suspending functions are designed to imitate
_sequential_ behavior by default.

Concurrency is hard, concurrency has to be explicit.

Kotlin:

Sequential behavior (default):
requestToken()            VALID -> produces Token

Concurrent behavior:
async { requestToken() }  VALID -> produces Deferred<Token>

*/

fun coroutines_vs_threads() {
    // Coroutines are like very light-weight threads.
    fun testCoroutines() = runBlocking {
        val jobs = List(100_000) {
            GlobalScope.launch {
                delay(1000L)
                print(".")
            }
        }
        jobs.forEach { it.join() }
    }

    // Will result in:
    // Exception in thread "main" java.lang.OutOfMemoryError: unable to create native thread:
    // possibly out of memory or process/resource limits reached
    fun testThreads() {
        val threads = List(100_000) {
            thread {
                Thread.sleep(1000L)
                print(".")
            }
        }
        threads.forEach { it.join() }
    }
}

suspend fun launch_vs_async(): String {
    // Creates, starts and returns a reference to the coroutine as a Job - (Lazy)StandaloneCoroutine
    val job: Job = GlobalScope.launch {
        // can't return from here, this block is: CoroutineScope.() -> Unit
    }
    // Creates, starts and returns coroutine as its future result - (Lazy)DeferredCoroutine
    val result1 = GlobalScope.async {
        // implicitly returns Deferred<Unit> to assign to result1
    }
    val result2: Deferred<String> = GlobalScope.async {
        delay(1000)
        "Hello"
    }
    return result2.await() // launch_vs_async (that must be suspending) will return when result2 is ready
}

fun awaiting() {
    runBlocking {
        (1..10).map {
            async {
                println(it)
                delay(5000)
                it.toFloat()
            }
        }.apply {
            // This will print somewhere in the middle of the Int numbers,
            // because the above block will return very quickly after
            // spawning 10 coroutines, and before some of them had a chance
            // to print the Int number they got.
            println("Done spawning. Awaiting...")
        }.map {
            it.await() // This will result in ~5s wait time, not 10 x 5s
        }.forEach {
            println(it)
        }
    }
}

data class Post(val hello: String)
data class Token(val hello: String)
data class Item(val hello: String)

fun String.toItem() = Item(this)

suspend fun requestToken(): Token { return Token("hello")
}
suspend fun createPost(token: Token, item: Item): Post { return Post("hello")
}
fun processPost(post: Post) {}

suspend fun <T> retryIO(block: suspend () -> T) : T {
    var curDelay = 1000L
    while (true) {
        try {
            return block()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        delay(curDelay)
        curDelay = (curDelay * 2).coerceAtMost(60_000L)
    }
}

fun testLaunch() {
    fun doSomething(block: suspend Int.() -> Float) {
        GlobalScope.launch { // start a coroutine in background and return from doSomething
            val float: Float = block(5)
            println("Float: $float") // won't print anything as it runs in a background thread pool
        }
    }

    fun doSomethingElse(block: Int.() -> Float) {
        GlobalScope.launch { // start a coroutine in background and return from doSomething
            val float: Float = block(5)
            println("Float: $float") // won't print anything as it runs in a background thread pool
        }
    }

    fun willNotPrint() {
        // The program will exit immediately, unless something else is keeping it busy,
        // allowing the launched coroutines to finish and print the messages.
        doSomething {
            (this + 2).toFloat()
        }
        doSomethingElse {
            (this + 3).toFloat()
        }
    }

    fun willPrint() {
        runBlocking {
            doSomething {
                println("Int: $this")
                this.toFloat()
            }
        }
    }

//    willNotPrint()
    willPrint()
}

suspend fun postItem(item: Item) {
    val token = requestToken()
    val post = createPost(token, item)
    processPost(post)

    val list = listOf(
        token to item
    )

    // No need for callbacks or special loop combinators (as is the case with Promises).
    for ((token, item) in list) {
        createPost(token, item)
    }

    try {
        createPost(token, item)
    } catch (e: Exception) {

    }

    val file = File("/")
    file.readLines().forEach { line ->
        createPost(token, line.toItem())
    }

    val post2 = retryIO {
        createPost(token, item)
    }
}

fun postItem2(item: Item) {
    // Using 'launch' coroutine builder, which takes a regular function
    // and turns it into a suspending lambda.

    // No need to make the postItem2 a suspend function.
    GlobalScope.launch { // return immediately, coroutines works in background thread pool
        val token = requestToken()
        val post = createPost(token, item)
        processPost(post) // will be executed in the UI thread
    }
}

/*

C# approach to async.

async Task<Image> loadImageAsync(String name) { ... }

var promise1 = loadImageAsync(name1);
var promise2 = loadImageAsync(name2);

var image1 = await promise1;
var image2 = await promise2;

var result = combineImages(image1, image2);

*/

/*

/Kotlin approach to async.

Using the 'Async' suffix to the let the user know the function is concurrent.
Deferred - Kotlin's future type.
Name 'Future' is already taken by Java.
'Promise' taken by JavaScript.
'async' is a coroutine builder.

fun loadImageAsync(name: String): Deferred<Image> = async { ... }

suspend fun testAsync() {
    val deferred1 = loadImageAsync(name1)
    val deferred2 = loadImageAsync(name2)

    val image1 = deferred1.await()
    val image2 = deferred2.await()

    val result = combineImages(image1, image2)
}

However, the above approach is not a good style in Kotlin.
Instead we would write:

suspend fun loadImage(name: String): Image { ... }

suspend fun loadAndCombine(name1: String, name2: String): Image {
    val deferred1 = async { loadImage(name1) }
    val deferred2 = async { loadImage(name2) }
    return combineImages(deferred1.await(), deferred2.await())

Integration with Java code.
In Java we'd do:

CompletableFuture<Image> loadImageAsync(String name) { ... }
CompletableFuture<Image> loadAndCombineAsync(String name1, String name2) {
    CompletableFuture<Image> future1 = loadImageAsync(name1);
    CompletableFuture<Image> future2 = loadImageAsync(name2);
    return future1.thenCompose(image1 ->
        future2.thenCompose(image2 ->
            CompletableFuture.supplyAsync(() ->
                combineImages(image1, image2))));
}

In Kotlin, we can do:

fun loadAndCombineAsync(name1: String, name2: String): CompletableFuture<Image> =
// future coroutine builder (returns a CompletableFuture and runs the coroutine inside of it)
future {
    val future1 = loadImageAsync(name1)
    val future2 = loadImageAsync(name2)
    combineImages(future1.await(), future2.await())
    // 'await' is an extension function for Java's CompletableFuture
}

*/

// Beyond coroutines.
val fibonacci = sequence { // synchronous with invoker (unlike 'launch')
    var cur = 1
    var next = 1
    while (true) {
        yield(cur)
        val tmp = cur + next
        cur = next
        next = tmp
    }
}

fun getInts(first: Int, last: Int) = sequence {
    for (i in first..last) {
        yield(i)
    }
}

fun testGetInts() = runBlocking {
    for (i in getInts(5, 10).iterator()) {
        println(i)
    }
}

fun testFibonacci() {
    val iter = fibonacci.iterator()

    // As soon as 'next()' is invoked the code inside buildSequence starts and runs until
    // the next suspension, which is 'yield'.
    println(iter.next()) // 1
    // The coroutine gets resumed again.
    println(iter.next()) // 1
    println(iter.next()) // 2
}

/*

Classic async:

Keywords:
async/await
generate/yield

Kotlin:

'suspend' modifier

*/

/*

Direct style:

fun postItem(item: Item) {
    val token = requestToken() // action (wait for token)
    val post = createPost(token, item)  -----> continuation
    processPost(post)                   __/
}

Continuation Passing Style (CPS):

// Continuation = what should happen after the action is done
fun postItem(item: Item) {
    requestToken { token -> // pass continuation as a parameter to your action
        val post = createPost(token, item)  -----> continuation
        processPost(post)                   __/
    }
}

CPS == Callbacks

Coroutines direct style:

suspend fun postItem(item: Item) {
    val token = requestToken()
    val post = createPost(token, item)
    processPost(post)
}

How does it work?

When suspending functions are compiled, they acquire an additional
Continuation parameter.

suspend fun createPost(token: Token, item: Item): Post { ... } // is turned into Java/JVM code like this

                                              callback
                                                 |
                                                 V
Object createPost(Token token, Item item, Continuation<Post> cont) { ... }

interface Continuation<in T> {  // Continuation is just a generic callback interface
    val context: CoroutineContext
    fun resume(value: T)
    fun resumeWithException(exception: Throwable)
}

So by using suspend functions we are just programming with callbacks behind the scenes.

You can do it yourself by hand, for example:

fun postItem(item: Item) {
    requestToken { token ->
        createPost(token, item) { post ->
            processPost(post)
        }
    }
}

But that's not what happens...

Identify suspension points:

suspend fun postItem(item: Item) {
    // LABEL 0 - initial continuation
    val token = requestToken()
    // LABEL 1
    val post = createPost(token, item)
    // LABEL 2
    processPost(post)
}

Then compiler transforms the above into a one big switch.
Conceptually (pseudo-code) it looks like this:

suspend fun postItem(item: Item, cont: Continuation) {
    // allocate an object to keep current execution state
    val sm = cont as? ThisSM ?: object : CoroutineImpl {
        fun resume(...) {
            // Invoke the postItem function again to get back
            // to our state machine code.
            postItem(null, this)
        }
    }
    switch (sm.label) {
        case 0:
            sm.item = item
            sm.label = 1     // the label of the next action we are going to take
            requestToken(sm) // sm - State Machine as Continuation
            // We pass 'sm' to the 'requestToken' so that it can call us back
            // when it is done by calling 'resume'.
        case 1:
            val item = sm.item
            val token = sm.result as Token
            sm.label = 2
            val post = createPost(token, item, sm)
        case 2:
            processPost(post)
    }
}

State Machine vs Callbacks.

Only one closure and an instance of a sm object are allocated and reused
on every suspension.

Whereas in the callback example a new closure is allocated each time:

fun postItem(item: Item) {
    requestToken { token ->
        createPost(token, item) { post ->
            processPost(post)
        }
    }
}

Easy loops.

suspend fun postItems(items: List<Item>) {
    for (item in items) {
        val token = requestToken()
        val post = createPost(token, item)
        processPost(post)
    }
}

*/

/*

Integration.

interface Service {
    fun createPost(token: Token, item: Item): Call<Post>
}

We want to convert the above into a suspending function:

suspend fun createPost(token: Token, item: Item): Post =
    serviceInstance.createPost(token, item).await()

suspend fun <T> Call<T>.await(): T = suspendCoroutine { cont ->
    // Every kind of async future has a method to install a callback.
    enqueue(object: Callback<T> { // in case of the Retrofit, it's 'enqueue'.
        override fun onResponse(call: Call<T>, response: Response<T>) {
            if (response.isSuccessful) {
                cont.resume(response.body()!!)
            else
                cont.resumeWithException(ErrorResponse(response))
        }
        override fun onFailure(call: Call<T>, t: Throwable) {
            cont.resumeWithException(t)
        }
    })
}

// 'suspendCoroutine' is a Kotlin standard library function.
// The opposite of coroutine builder.
suspend fun <T> suspendCoroutine(block: (Continuation<T>) -> Unit): T

It will suspend the execution of the current code, and will represent
whatever follows this code as a Continuation object, and pass it to the
block of code inside of it.

Out of the box integrations in kotlinx-coroutines-core:
- rx1
- rx2
- reactor
- nio
- guava
- jdk8

*/

/*

Coroutine context.

suspend fun postItem(item: Item) {
    val token = requestToken()           -----> suspending functions
    val post = createPost(token, item)   __/
    processPost(post)  // what thread it resumes on?

    // It depends on exact implementation details of the 'createPost' function.
    // If 'createPost' uses Retrofit, the Retrofit will invoke the callback
    // later in one of its own threads.

    // But what if we want to execute it in the UI thread because we want to
    // update UI?
}

The way it works...

The context of a coroutine is just a map of elements, where each element has a key.
An interception is just one of those elements in a context.

interface ContinuationInterceptor : CoroutineContext.Element {
    // Used to retrieve the instance of interceptor from the context.
    companion object Key : CoroutineContext.Key<ContinuationInterceptor>

    // Installs a custom continuation wrapper that do with continuations whatever
    // they want, like execute our code on the thread that we control.
    fun <T> interceptContinuation(continuation: Continuation<T>): Continuation<T>
}

class DispatchedContinuation<in T>(
    val dispatcher: CoroutineDispatcher,
    val continuation: Continuation<T>
): Continuation<T> by continuation {

    // Instead of resuming code directly and immediately invoking your code,
    // it submits the task to a separate thread.
    override fun resume(value: T) {
        dispatcher.dispatch(context, DispatchTask(...))
    }

    // In normal code without the interceptor, invoking 'resume' passes control
    // to whatever code you have in your coroutine.
    // In this case, invoking 'resume' returns immediately, but the task is
    // dispatched to the corresponding thread. Usually that's UI thread or
    // some thread pool.

    ...
}

*/

/*

Starting coroutines.

fun <T> future(
    context: CoroutineContext = DefaultDispatcher,
    block: suspend () -> T
): CompletableFuture<T> {  // But how do I return my own future type?
    val future = CompletableFuture<T>()
    block.startCoroutine(completion = object : Continuation<T> {
        // We have context as a parameter to our builder.
        override val context: CoroutineContext get() = context

        override fun resume(value: T) {
            future.complete(value)
        }

        override fun resumeWithException(exception: Throwable) {
            future.completeExceptionally(exception)
        }
    })
    return future
}

*/

/*

Job cancellation.

val job = launch { ... }

job.join()    // wait for job completion
job.cancel()

interface Job : CoroutineContext.Element {
    companion object Key : CoroutineContext.Key<Job>
    ...
}

Job is an element of coroutine context.
It means that we are inside the coroutine.

launch {
    val job = coroutineContext[Job]!!
    val interceptor = coroutineContext[CoroutineInterceptor]!!
}

launch {
    withTimeout(10, TimeUnit.Seconds) {
        ...
    }
}


Cooperative cancellation.

launch { // not cancellable
    while(true) { ... } // this code does not cooperate
}

launch { // cancellable
    while (isActive) { ... }
}

launch { // cancellable
    while(true) {
        delay(...)
    }
}

What if I'm writing my future and I'm writing a suspending function to wait for it.
How do I cancel it?

suspend fun <T> Call<T>.await(): T =
    suspendCancellableCoroutine { cont ->
        enqueue(...)
        // Not a simple continuation, but a cancellable one, that lets you
        // install a handler that will be called on cancellation or other kind
        // of completion.
        cont.invokeOnCompletion {
            this@await.cancel() // the 'cancel' in this case is a method of a Retrofit call
        }
    }

Same as suspendCoroutine, but makes it cancellable.

Cancellation is not just about aborting the work, but also about cleaning resources.

*/

/*

Communicating Sequential Processes (CSP) = no shared state

Shared Mutable State   < -vs- >   Share by Communicating

Using named channels with anonymous coroutines.

*/

fun testCSP() = runBlocking {
    val chan = Channel<Int>() // object that we can send the data to
    launch(coroutineContext) { // child coroutine
        repeat(10) { i ->      // \
            delay(100)         //  |
            chan.send(i)       //  |--- sequential code
        }                      //  |
        chan.close()           // /
    }
    launch(coroutineContext) {
        for (i in chan) {
            println(i)
        }
    }
}

/*

// Go version:

fun main() {
    messages := make(chan string)
    go func() { messages <- "ping" }()
    msg := <- messages
    fmt.Println(msg)
}

*/
fun testGoCSP() = runBlocking {
    val messages = Channel<String>()
    launch {
        messages.send("ping")
    }
//    messages.receive()
    println(messages.first())
}

/*

Actors (another variant of CSP)

Named channels   < -vs ->   Name coroutines

Actor == a pair of a named coroutine and its inbox channel

*/

// Instead of creating a channel explicitly, we use the 'actor' builder.

fun testActor() = runBlocking {
    val printer = actor<Int>(coroutineContext) {
        for (i in channel) {
            println(i)
        }
    }
    launch(coroutineContext) {
        repeat(10) { i ->
            delay(100)
            printer.send(i)
        }
        printer.close()
    }
}

fun testAwait() = runBlocking {
    // Rule of thumb: async all then await all (not async/await one by one)

    // Asynchronous
    (0..9).map {
        async {
            delay(500)
            println(it)
        }
    }.map {
        it.await()
    }

    // Sequential
    (0..9).map {
        async {
            delay(500)
            println(it)
        }.await()
    }

    // Sequential
    val n0 = async {
        delay(1000)
        println(0)
    }.await()
    val n1 = async {
        delay(1000)
        println(1)
    }.await()
    val n2 = async {
        delay(1000)
        println(2)
    }.await()
    println("$n0 $n1 $n2")

    // Asynchronous
    val n3 = async {
        delay(1000)
        println(3)
    }
    val n4 = async {
        delay(1000)
        println(4)
    }
    val n5 = async {
        delay(1000)
        println(5)
    }

    println("${n3.await()} ${n4.await()} ${n5.await()}")
}

fun testSuspend() = runBlocking {
    val listJob = async { suspendFun() }
    val minusList = (0..9).map {
        async {
            delay(500)
            it * -10
        }
    }.map {
        it.await()
    }
    println(listJob.await())
    println(minusList)
}

suspend fun suspendFun(): List<Int> {
    return (0..9).map {
        GlobalScope.async {
            delay(500)
            it * 10
        }
    }.map {
        it.await()
    }
}


fun main(args: Array<String>) {
    testSuspend()
//    testAwait()
//    testLaunch()
//    testCSP()
//    testActor()
//    testGoCSP()
}