package com.vitalyk.kotlin.sandbox.coroutines.ui

import javafx.application.Application
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.text.Text
import javafx.stage.Stage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.delay
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import tornadofx.*

// https://github.com/Kotlin/kotlinx.coroutines/blob/master/ui/coroutines-guide-ui.md

fun main(args: Array<String>) {
    Application.launch(ExampleApp::class.java, *args)
}

class ExampleApp : Application() {
    val hello = Text("Hello World!").apply {
        fill = Color.valueOf("#C0C0C0")
    }

    // floating action button
    val fab = Circle(20.0, Color.valueOf("#FF4081")).apply {
        hoverProperty().onChange {
            fill = if (it) Color.YELLOW else Color.valueOf("#FF4081")
        }
    }

    val root = StackPane().apply {
        children += hello
        children += fab
        StackPane.setAlignment(hello, Pos.CENTER)
        StackPane.setAlignment(fab, Pos.BOTTOM_RIGHT)
        StackPane.setMargin(fab, Insets(15.0))
    }

    val scene = Scene(root, 240.0, 380.0).apply {
        fill = Color.valueOf("#303030")
    }

    override fun start(stage: Stage) {
        stage.title = "Example"
        stage.scene = scene
        stage.show()
        actorSetup(hello, fab)
    }
}

fun basicSetup(hello: Text, fab: Circle) {
    // Coroutines confined to the UI thread can freely update anything in UI
    // and suspend without blocking the UI thread.
    val job = GlobalScope.launch(Dispatchers.JavaFx) { // launch coroutine in UI context
        for (i in 10 downTo 1) { // countdown from 10 to 1
            hello.text = "Countdown $i ..." // update text
            delay(500) // wait half a second
            // UI is not frozen while delay waits,
            // because it does not block the UI thread -- it just suspends the coroutine.
        }
        hello.text = "Done!"
    }

    fab.onMouseClicked = EventHandler {
        job.cancel() // Completely thread-safe and non-blocking.
        // Signals the coroutine to cancel its job,
        // without waiting for it to actually terminate.
        // Can be invoked from anywhere.
        // Invoking it on a coroutine that was already cancelled
        // or has completed does nothing.
    }
}

fun naiveSetup(hello: Text, fab: Circle) {
    // Launches a new coroutine on each circle click,
    // launched coroutines then compete to update the text.
    fab.onNaiveClick {
        for (i in 10 downTo 1) {
            hello.text = "Countdown $i ..."
            delay(500)
        }
        hello.text = "Done!"
    }
}

fun Node.onNaiveClick(action: suspend (MouseEvent) -> Unit) {
    onMouseClicked = EventHandler { event ->
        GlobalScope.launch(Dispatchers.JavaFx) {
            action(event)
        }
    }
}

fun actorSetup(hello: Text, fab: Circle) {
    // The clicks are ignored while the countdown animation is running.
    // This happens because the actor is busy with an animation
    // and does not receive from its channel.
    // By default, an actor's mailbox is backed by RendezvousChannel,
    // whose offer operation succeeds only when the receive is active.
    fab.onClick {
        for (i in 10 downTo 1) {
            hello.text = "Countdown $i ..."
            delay(500)
        }
        hello.text = "Done!"
    }
}

// We can cancel an active job before starting a new one to ensure
// that at most one coroutine is animating the countdown.
// However, it is generally not the best idea. The cancel function serves
// only as a signal to abort a coroutine. Cancellation is cooperative
// and a coroutine may, at the moment, be doing something non-cancellable
// or otherwise ignore a cancellation signal. A better solution is to use
// an actor for tasks that should not be performed concurrently.
fun Node.onClick(action: suspend (MouseEvent) -> Unit) {
    // launch one actor to handle all events on this node
    val eventActor = GlobalScope.actor<MouseEvent>(Dispatchers.JavaFx) {
        for (event in channel) action(event)
    }
    // install a listener to offer events to this actor
    onMouseClicked = EventHandler { event ->
        eventActor.offer(event) // sends an element to the actor immediately,
                                // if it is possible, or discards an element otherwise
    }
}