package com.vitalyk.kotlin.sandbox.coroutines

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.Pane
import javafx.stage.Stage
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx

class Test2: Application() {
    override fun start(primaryStage: Stage) {
        val button = Button("Hello")

        val scene = Scene(Pane(button))
        primaryStage.scene = scene
        primaryStage.title = "Basic JavaFx app"
        primaryStage.show()

        // Any blocking operation inside `launch` will halt the UI thread
        // if JavaFx context is used.
        GlobalScope.launch(Dispatchers.JavaFx) {
            while (isActive) {
                val num = {
                    var a = Math.random()
                    for (i in 1..1000_000_000) {
                        a += 1
                    }
                    a
                }()
                delay(1000 * 3)
            }
        }

        // Also see:
        // https://stackoverflow.com/questions/48935980/why-this-kotlin-coroutine-is-freezing-the-interface
    }
}

fun main(args: Array<String>) {
    Application.launch(Test2::class.java, *args)
}