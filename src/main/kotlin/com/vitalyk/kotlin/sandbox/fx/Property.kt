package com.vitalyk.kotlin.sandbox.fx

import javafx.application.Application
import javafx.beans.property.*
import javafx.collections.FXCollections
import javafx.scene.Scene
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.VBox
import javafx.stage.Stage
import java.beans.PropertyChangeListener
import java.beans.PropertyChangeSupport

// JavaBeans properties.

// Types of properties: regular, indexed, bound, constrained.
// Read-write, read-only or write-only.

class MyBean {
    private val pcs = PropertyChangeSupport(this) // this instance is thread-safe

    fun addPropertyChangeListener(listener: PropertyChangeListener) {
        pcs.addPropertyChangeListener(listener)
    }

    fun removePropertyChangeListener(listener: PropertyChangeListener) {
        pcs.removePropertyChangeListener(listener)
    }

    // As answered by `yole` from JetBrains Team on Oct 2016:
    // At this time Kotlin supports neither write-only properties,
    // nor parameter type overloading for the setter.
    // It’s possible that either of those features will be added to a future version of Kotlin,
    // even though they’re not on the 1.1 roadmap.

    var value: String? = null // bound property = setter generates property change notifications
        // That's how you do it in Kotlin ¯\_(ツ)_/¯
        @Deprecated(message = "Write only property.", level = DeprecationLevel.ERROR)
        get() = field
        set(value) {
            val oldValue = field
            field = value
            pcs.firePropertyChange("value", oldValue, value)
        }
}

fun testBean() {
    val bean = MyBean()

    bean.addPropertyChangeListener(PropertyChangeListener {
        println(it.propertyName + " changed: " + it.oldValue + " -> " + it.newValue)
    })

    bean.value = "Vitaly"
    bean.value = "Kravchenko"
    bean.value = "Kravchenko" // no change notification is sent

//    println(bean.value) // write-only
}

// JavaFx properties.

// All properties are observable.
// Read-write or read-only.
// Properties are objects.

class MyFxBean {
    // The default value for the initial value depends on the type of the property.
    // It is zero for numeric types, false for boolean types, and null for reference types.
    val counter = SimpleIntegerProperty(100) // initial value

    // Working with read-only properties is a bit tricky.
    // A ReadOnlyXXXWrapper class wraps two properties of XXX type:
    // one read-only and one read/write.
    // The wrapper is typically made private, for class to set the name value internally.
    val nameWrapper = ReadOnlyStringWrapper("Vitaly")
    val name = nameWrapper.readOnlyProperty
}

fun testFxBean() {
    val bean = MyFxBean()

    println(bean.counter)
    // `value` below is property access syntax for `getValue()`,
    // in other words, get() == getValue()
    assert(bean.counter.get() == bean.counter.value)
    bean.counter.addListener { observable, oldValue, newValue ->
        println("$observable: $oldValue -> $newValue")
    }
    bean.counter.value++
    bean.counter.set(42)

    println(bean.name.value)
    assert(bean.name.value == bean.nameWrapper.value)
    bean.name.addListener { observable, oldValue, newValue ->
        println("$observable: $oldValue -> $newValue")
    }
//    bean.name.value = "Kravchenko" // read-only
    bean.nameWrapper.value = "Kravchenko"

    // A property object wraps three pieces of information:
    // - the reference of the bean that contains it
    // - a name
    // - a value
    assert(bean.counter.name == "")   // default name
    assert(bean.counter.bean == null) // default bean
}

class Book {
    //                                       bean,  name,    initial value
    private val title = SimpleStringProperty(this, "title", "Unknown")
    fun titleProperty(): StringProperty {
        return title
    }
    // According to the JavaFX design patterns, and not for any technical requirements,
    // (well, mostly to make the class interoperable with older tools)
    // a JavaFX property has a getter and a setter
    // that are similar to the getters and setters in JavaBeans.
    fun getTitle(): String {
        return title.get()
    }
    fun setTitle(title: String) {
        this.title.set(title)
    }
}

class SimpleBook {
    val titleProperty = SimpleStringProperty()
    var title: String
        get() = titleProperty.get()
        set(value) = titleProperty.set(value)
}

class SimplerBook {
    val titleProperty = SimpleStringProperty()
}

class MyApp : Application() {
    override fun start(stage: Stage) {
        val root = VBox()

        root.children += TableView<Book>().apply {
            val titleColumn = TableColumn<Book, String>("Title")
            titleColumn.setCellValueFactory { cellDataFeatures -> Book::titleProperty.call(cellDataFeatures.value) }
            columns += titleColumn

            val book = Book().apply {
                setTitle("Red Rising")
            }
            items = FXCollections.observableList(listOf(book))

        }

        root.children += TableView<SimpleBook>().apply {
            val titleColumn = TableColumn<SimpleBook, String>("Title")
            titleColumn.setCellValueFactory { SimpleBook::titleProperty.call(it.value) }
            columns += titleColumn

            val simpleBook = SimpleBook().apply {
                title = "And Then There Were None"
            }
            items = FXCollections.observableList(listOf(simpleBook))

        }

        root.children += TableView<SimplerBook>().apply {
            val titleColum = TableColumn<SimplerBook, String>("Title")
            titleColum.setCellValueFactory { SimplerBook::titleProperty.call(it.value) }
            columns += titleColum

            val simplerBook = SimplerBook().apply {
                titleProperty.value = "Anna Karenina"
            }
            items = FXCollections.observableList(listOf(simplerBook))
        }

        val scene = Scene(root, 400.0, 400.0)
        stage.scene = scene

        stage.show()
    }
}

fun testBooks() {
    Application.launch(MyApp::class.java)
}

fun bidiBindings() {
    val prop = SimpleBooleanProperty(false)
    val prop1 = SimpleBooleanProperty(true)
    val prop2 = SimpleBooleanProperty(true)

    prop1.bindBidirectional(prop)
    prop2.bindBidirectional(prop)

    println(prop.value)
    println(prop1.value)
    println(prop2.value)

    println()

    prop1.set(true)
    println(prop.value)
    println(prop1.value)
    println(prop2.value)
}

fun main(args: Array<String>) {
//    testBean()
//    testFxBean()
//    testBooks()
    bidiBindings()
}