package com.vitalyk.kotlin.sandbox

import java.util.stream.Collectors

data class Person(val firstName: String, val lastName: String)

fun main(args: Array<String>) {
    val people = listOf(
        Person("Vitaly", "Kravchenko"),
        Person("Bob", "Marley"),
        Person("Bill", "Clinton")
    )

//    val str = people.stream().map(Person::toString).collect(Collectors.joining("\n"))
    val str = people.parallelStream().map(Person::toString).collect(Collectors.joining("\n"))

    println(str)
}