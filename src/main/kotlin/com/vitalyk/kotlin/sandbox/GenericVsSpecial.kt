package com.vitalyk.kotlin.sandbox

import kotlin.system.measureNanoTime

fun main(args: Array<String>) {
//    testCollection()
    testParams()
}

fun testCollection() {
    println("Setting up...")

    val size = 10_000_000
    val doubles = mutableListOf<Double>()
    (0 until size).forEach {
        doubles.add(Math.random())
    }

    val genericDoubles: List<Double> = doubles.toList()
    val specificDoubles: DoubleArray = doubles.toDoubleArray()

    println("Testing...\n")

    for (i in 0..9) {
        val genericTime = measureNanoTime {
            var sum = 0.0
            for (d in genericDoubles) {
                sum += d
            }
        }
        val specificTime = measureNanoTime {
            var sum = 0.0
            for (d in specificDoubles) {
                sum += d
            }
        }
        println("Generic: $genericTime\nSpecific: $specificTime\n")
    }

    println("-------------------------------------------")

    for (i in 0..9) {
        val specificTime = measureNanoTime {
            var sum = 0.0
            for (d in specificDoubles) {
                sum += d
            }
        }
        val genericTime = measureNanoTime {
            var sum = 0.0
            for (d in genericDoubles) {
                sum += d
            }
        }
        println("Specific: $specificTime\nGeneric: $genericTime\n")
    }

    println("-------------------------------------------")

    for (i in 0..9) {
        val specificTime = measureNanoTime {
            var sum = 0.0
            for (d in specificDoubles) {
                sum += d
            }
        }
        println("Specific: $specificTime")
    }

    for (i in 0..9) {
        val genericTime = measureNanoTime {
            var sum = 0.0
            for (d in genericDoubles) {
                sum += d
            }
        }
        println("Generic: $genericTime")
    }
}

fun <T> genericParam(a: T, b: T, flag: Boolean): T {
    return if (flag) a else b
}

fun specificParam(a: Double, b: Double, flag: Boolean): Double {
    return if (flag) a else b
}

fun <T> nullableGenericParam(a: T?, b: T?, flag: Boolean): T? {
    return if (flag) a else b
}

fun nullableSpecificParam(a: Double?, b: Double?, flag: Boolean): Double? {
    return if (flag) a else b
}

fun testParams() {
    println("Setting up...")
    val size = 10_000_000
    val doubles = mutableListOf<Double>()
    (0 until size).forEach {
        doubles.add(Math.random())
    }

    val specificDoubles: DoubleArray = doubles.toDoubleArray()

    // The performance of both generic and specific methods is about the same.

    for (i in 0..9) {
        val specificTime = measureNanoTime {
            var sum = 0.0
            for (d in 0 until size step 2) {
                sum += specificParam(specificDoubles[d], specificDoubles[d + 1], d % 2 == 0)
            }
        }
        println("Specific: $specificTime")
    }

    println("-------------------------------------------")

    for (i in 0..9) {
        val genericTime = measureNanoTime {
            var sum = 0.0
            for (d in 0 until size step 2) {
                sum += genericParam(specificDoubles[d], specificDoubles[d + 1], d % 2 == 0)
            }
        }
        println("Generic: $genericTime")
    }

    println("-------------------------------------------")

    // Both nullable versions are more than an order of magnitude slower.

    for (i in 0..9) {
        val specificTime = measureNanoTime {
            var sum = 0.0
            for (d in 0 until size step 2) {
                sum += nullableSpecificParam(specificDoubles[d], specificDoubles[d + 1], d % 2 == 0) ?: 0.0
            }
        }
        println("Nullable Specific: $specificTime")
    }

    println("-------------------------------------------")

    for (i in 0..9) {
        val genericTime = measureNanoTime {
            var sum = 0.0
            for (d in 0 until size step 2) {
                sum += nullableGenericParam(specificDoubles[d], specificDoubles[d + 1], d % 2 == 0) ?: 0.0
            }
        }
        println("Nullable Generic: $genericTime")
    }
}