package com.vitalyk.kotlin.sandbox

import java.util.Random
import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.stage.Stage

// https://blog.netopyr.com/2012/06/14/using-the-javafx-animationtimer/
class FXSandbox : Application() {

    private val STAR_COUNT = 20000
    private val nodes = arrayOfNulls<Rectangle>(STAR_COUNT)
    private val angles = DoubleArray(STAR_COUNT)
    private val start = LongArray(STAR_COUNT)

    private val random = Random()

    override fun start(primaryStage: Stage) {
        for (i in 0..STAR_COUNT - 1) {
            nodes[i] = Rectangle(1.0, 1.0, Color.WHITE)
            angles[i] = 2.0 * Math.PI * random.nextDouble()
            start[i] = random.nextInt(2000000000).toLong()
        }
        val scene = Scene(Group(*nodes), 800.0, 600.0, Color.BLACK)
        primaryStage.scene = scene
        primaryStage.show()

        object : AnimationTimer() {
            override fun handle(now: Long) {
                val width = 0.5 * primaryStage.width
                val height = 0.5 * primaryStage.height
                val radius = Math.sqrt(2.0) * Math.max(width, height)
                for (i in 0..STAR_COUNT - 1) {
                    val node = nodes[i]
                    val angle = angles[i]
                    val t = (now - start[i]) % 2000000000
                    val d = t * radius / 2000000000.0
                    node?.setTranslateX(Math.cos(angle) * d + width)
                    node?.setTranslateY(Math.sin(angle) * d + height)
                }
            }
        }.start()
    }

}

fun main(args: Array<String>) {
    Application.launch(FXSandbox::class.java, *args)
}