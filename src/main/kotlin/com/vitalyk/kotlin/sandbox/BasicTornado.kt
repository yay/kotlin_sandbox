package com.vitalyk.kotlin.sandbox

import javafx.animation.Interpolator
import javafx.application.Application
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import tornadofx.*

class MyView: View() {
    override val root = VBox()

    val colors = arrayOf(
        "#3CE02E",
        "#0F99FB",
        "#D600D5",
        "#FF5716",
        "#FFDE03",
        "#FF1001"
    )

    init {
        primaryStage.minWidth = 400.0
        primaryStage.minHeight = 600.0

        with (root) {
            button("Generate Palette") {
                minWidth = primaryStage.minWidth
                setOnAction {
                    this.isDisable = true
                    renderColors()
                }
            }
        }
    }

    fun renderColors() {
        colors.forEach { color -> renderColor(color) }
    }

    fun renderColor(color: String) {
        with (root) {
            val rect = rectangle {
                width = root.width
                height = 50.0
                fill = Color.web(color)
            }
            rect.setOnMouseMoved {
                rect.scaleXProperty().set(1.0)
                timeline {
                    keyframe(0.3.seconds) {
                        keyvalue(rect.scaleXProperty(), 0.7, interpolator = Interpolator.EASE_OUT)
                    }
                    isAutoReverse = true
                    cycleCount = 2
                }
            }
        }
    }
}

class BasicTornadoApp: App(MyView::class)

fun main(args: Array<String>) {
    Application.launch(BasicTornadoApp::class.java, *args)
}