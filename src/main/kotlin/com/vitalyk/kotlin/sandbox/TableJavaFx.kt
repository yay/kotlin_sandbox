package com.vitalyk.kotlin.sandbox

import com.sun.javafx.scene.control.skin.LabeledText
import javafx.animation.FadeTransition
import javafx.application.Application
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.stage.Stage
import javafx.util.Duration
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import tornadofx.*
import java.util.*

// Only the column created using DSL updates.
class Entity(size: Double) {
    val sizeProperty = SimpleDoubleProperty(size)
    var size by sizeProperty
}

// // Both columns update.
// class Entity(size: Double) {
//     val _sizeProperty = SimpleDoubleProperty(size)
//     fun sizeProperty() = _sizeProperty
//     var size by _sizeProperty
// }

class TestView : View() {
//    val entities = FXCollections.observableArrayList<Entity>(Entity(5.0))
    val entities = mutableListOf(Entity(5.0)).observable()

    override val root = vbox {
//        tableview(entities) {
//            // DSL column
//            column("Size", Entity::sizeProperty)
//
//            // Old school column
//            val sizeColumn = TableColumn<Entity, Double>("Size")
//            sizeColumn.cellValueFactory = PropertyValueFactory<Entity, Double>("size")
//            columns.add(sizeColumn)
//        }

        this += TableView<Entity>(entities).apply {
            val sizeColumn = TableColumn<Entity, Double>("Size")
//            sizeColumn.cellValueFactory = PropertyValueFactory<Entity, Double>("size")
            sizeColumn.setCellValueFactory { it.value.sizeProperty.asObject() }
            columns.add(sizeColumn)
        }
    }

    init {
        GlobalScope.launch {
            while(isActive) {
                delay(1000)
                runLater {
                    entities.first().size += -1.0 + Math.random() * 2.0
                }
            }
        }
    }
}

class TestApp: App(TestView::class)

fun main(args: Array<String>) {
    Application.launch(TestApp::class.java, *args)
}


class Human {
    val firstNameProperty = SimpleStringProperty()
    var firstName: String
        get() = firstNameProperty.get()
        set(value) = firstNameProperty.set(value)

    val lastNameProperty = SimpleStringProperty()
    var lastName: String
        get() = lastNameProperty.get()
        set(value) = lastNameProperty.set(value)

    val ageProperty = SimpleIntegerProperty()
    var age: Int
        get() = ageProperty.get()
        set(value) = ageProperty.set(value)

    val weightProperty = SimpleDoubleProperty()
//    fun weightProperty() = weightProperty
    var weight by weightProperty
//    var weight: Double
//        get() = weightProperty.get()
//        set(value) = weightProperty.set(value)
}


class TableApp: Application() {
    override fun start(primaryStage: Stage) {
        val humans = FXCollections.observableArrayList(
            Human().apply {
                firstName = "Vitaly"
                lastName = "Kravchenko"
                age = 32
                weight = 75.0
            },
            Human().apply {
                firstName = "Larisa"
                lastName = "Kravchenko"
                age = 55
                weight = 60.0
            },
            Human().apply {
                firstName = "Sergey"
                lastName = "Kravchenko"
                age = 55
                weight = 90.0
            }
        )

        val table = TableView<Human>(humans)

        // One way of doing it:
        val firstNameColumn = TableColumn<Human, String>("First Name")
        // populate column values using reflection
        firstNameColumn.cellValueFactory = PropertyValueFactory<Human, String>("firstName")
        // No cellFactory provided. Using DEFAULT_CELL_FACTORY.

        // Another way of doing it:
        val lastNameColumn = TableColumn<Human, String>("Last Name")
        // populate column values manually
        lastNameColumn.setCellValueFactory { cellDataFeatures: TableColumn.CellDataFeatures<Human, String> ->
            ReadOnlyStringWrapper(cellDataFeatures.value.lastName)
        }
        // No cellFactory provided. Using DEFAULT_CELL_FACTORY.

        val ageColumn = TableColumn<Human, Int>("Age")
        ageColumn.cellValueFactory = PropertyValueFactory<Human, Int>("age") // populates column values
        ageColumn.setCellFactory { column: TableColumn<Human, Int> ->        // renders column cells
            object : TableCell<Human, Int>() {
                override fun updateItem(item: Int?, empty: Boolean) {
                    super.updateItem(item, empty)

                    if (empty || item == null) {
                        text = null
                        graphic = null
                    } else {
                        text = "Human's age is $item"
                    }
                }
            }
        }

        // A cell factory must create new TableCell instances on each invocation,
        // but it's possible to have a single graphic for all returned cells and update
        // it on each factory invocation. However, if one needs to do something fancy,
        // like animating cells, a single graphic won't do, and each created cell
        // will need its own graphic created (at least per row).

        val weightColumn = TableColumn<Human, Double>("Weight")
        weightColumn.cellValueFactory = PropertyValueFactory<Human, Double>("weight")
        weightColumn.setCellFactory { column: TableColumn<Human, Double> ->
            FlashingTableCell<Human, Double>(
                nullsLast<Double>() as Comparator<Double>,
                { "%.2f".format(it) }
            )
//            weightColumn.properties.getOrPut("weightCell") {
//                FlashingTableCell<Human, Double>(nullsLast<Double>() as Comparator<Double>)
//            } as TableCell<Human, Double>
        }

//        table.column("Tornado", Human::weightProperty) {
//            val comparator = nullsLast<Double>() as Comparator<Double>
//            cellFormat {
//                val cell = this
//                graphic = cache {
//                    cell.itemProperty().addListener(ChangeListener { observable, oldValue, newValue ->
//                        val result = comparator.compare(
//                            newValue?.toDouble() ?: 0.0,
//                            oldValue?.toDouble() ?: 0.0
//                        )
//                        if (result > 0) {
//                            (graphic as FlashingTableCellGraphic).increase()
//                        } else if (result < 0) {
//                            (graphic as FlashingTableCellGraphic).decrease()
//                        }
//                    })
//                    FlashingTableCellGraphic(this@cellFormat)
//                }
//                text = "%.2f".format(item)
//            }
//        }

//        val flashingCell = FlashingTableCell<Human, Double>(nullsLast<Double>() as Comparator<Double>)
//        weightColumn.setCellFactory { flashingCell }

//        weightColumn.cellFormat {
//            val g = cache {
//                Label()
//            }
//            graphic = g
//            g.text = item.toString()
//            timeline {
//                keyframe(Duration.seconds(0.3)) {
//                    keyvalue(g.scaleXProperty(), 0.5)
//                }
//            }
//
//        }

        table.columns.addAll(firstNameColumn, lastNameColumn, ageColumn, weightColumn)

        val scene = Scene(table, 400.0, 200.0)
        primaryStage.scene = scene
        primaryStage.title = "Basic JavaFx app"
        primaryStage.show()

        GlobalScope.launch {
            while (isActive) {
                delay(1000)
                val index = Math.floor(Math.random() * 3).toInt()
                runLater {
                    humans[index].weight += - 1.0 + Math.random() * 2.0
                }
//                humans[index].weightProperty.value += - 1.0 + Math.random() * 2.0
//                humans[index] = Human().apply {
//                    firstName = human.firstName
//                    lastName = human.lastName
//                    age = human.age
//                    weight = human.weight - 1.0 + Math.random() * 2.0
//                }
            }
        }
    }
}

class FlashingTableCellGraphic(cell: Labeled) : StackPane() {
    val bgIncrease = Background(BackgroundFill(INCREASE_HIGHLIGHT_COLOR, CornerRadii.EMPTY, Insets.EMPTY))
    val bgDecrease = Background(BackgroundFill(DECREASE_HIGHLIGHT_COLOR, CornerRadii.EMPTY, Insets.EMPTY))
    val bgChange = Background(BackgroundFill(HIGHLIGHT_COLOR, CornerRadii.EMPTY, Insets.EMPTY))

    val background = BorderPane()
    val labeledText = LabeledText(cell)
    val transition = FadeTransition(HIGHLIGHT_TIME, background) // animates opacity

    init {
        labeledText.textProperty().bindBidirectional(cell.textProperty())
        alignment = cell.alignment
        children.addAll(background, labeledText)
    }

    fun increase() {
        background.background = bgIncrease
        animate()
    }

    fun decrease() {
        background.background = bgDecrease
        animate()
    }

    fun change() {
        background.background = bgChange
        animate()
    }

    private fun animate() {
        transition.fromValue = 1.0
        transition.toValue = 0.0
        transition.cycleCount = 1
        transition.isAutoReverse = false
        transition.playFromStart()
    }

    companion object {
        private val INCREASE_HIGHLIGHT_COLOR = Color.rgb(0, 255, 0, 0.8)
        private val DECREASE_HIGHLIGHT_COLOR = Color.rgb(255, 0, 0, 0.8)
        private val HIGHLIGHT_COLOR = Color.rgb(255, 255, 0, 0.8)
        private val HIGHLIGHT_TIME = Duration.millis(300.0)
    }
}

class FlashingTableCell<S, T>(
    private val comparator: Comparator<T>? = null,
    private val format: (T) -> String
) : TableCell<S, T>() {

    init {
        // We'll set both the `text` and the `graphic`, but only want
        // the graphic visible. The `textProperty` of the cell is going
        // to be used by the graphic.
        contentDisplay = ContentDisplay.GRAPHIC_ONLY

//        itemProperty().addListener(ChangeListener { obs, oldItem, newItem ->
//        })
    }

    override fun updateItem(field: T?, empty: Boolean) {
        val prevField = item

        super.updateItem(field, empty)

        if (empty || field == null) {
            text = null
            graphic = null
        } else if (tableRow != null) {
            text = format(field)

            val g = tableColumn.properties.getOrPut(tableRow) {
                FlashingTableCellGraphic(this)
            } as FlashingTableCellGraphic
            graphic = g

//            @Suppress("UNCHECKED_CAST")
//            val record = tableRow?.item as? S // record associated with the cell

            /*
             * We check that the value has been updated and that the row model/field
             * under the cell is the same. JavaFX table reuses cells so field is not
             * always the same!
             */
            val fieldChanged = prevField == null || prevField.hashCode() != field.hashCode()

            if (fieldChanged) {
                if (comparator != null) {
                    val compare = comparator.compare(field, prevField)
                    if (compare > 0) {
                        g.increase()
                    } else if (compare < 0) {
                        g.decrease()
                    }
                } else {
                    g.change()
                }
            }
        }
    }
}

//fun main(args: Array<String>) {
//    Application.launch(TableApp::class.java, *args)
//}