package com.vitalyk.kotlin.sandbox

import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Arc
import javafx.scene.shape.Circle
import javafx.stage.Stage

class BasicApp: Application() {

//    companion object {
//        @JvmStatic
//        fun main(args: Array<String>) {
//            Application.launch(BasicApp::class.java, *args)
//        }
//    }

    override fun start(primaryStage: Stage) {
        val canvas = Canvas().apply {
            val ctx = this.graphicsContext2D
            width = 400.0
            height = 400.0

            onMouseDragged = EventHandler { e ->
                ctx.clearRect(0.0, 0.0, ctx.canvas.width, ctx.canvas.height)

                ctx.stroke = Color.RED
                ctx.setLineDashes(4.0, 2.0)

                ctx.beginPath()
                ctx.moveTo(e.x, 0.0)
                ctx.lineTo(e.x, ctx.canvas.height)

                ctx.moveTo(0.0, e.y)
                ctx.lineTo(ctx.canvas.width, e.y)

                ctx.stroke()
            }
        }

        // Group extends Parent
        // Parent - The base class for all nodes that have children in the scene graph.
        val group = Group()
        val circle = Circle().apply {
            radius = 30.0
        }
        val arc = Arc().apply {
            centerX = 0.0
            centerY = 0.0
            radiusX = 100.0
            radiusY = 50.0
            startAngle = 90.0
            length = 270.0
        }
        group.children.add(circle)
//        group.children.add(arc)
        // scene and parent are both Node properties
        // Node - Base class for scene graph nodes. A scene graph is a set of tree data structures
        // * where every item has zero or one parent, and each item is either
        // * a "leaf" with zero sub-items or a "branch" with zero or more sub-items.
        println("Circle's scene: ${circle.scene}")
        println("Circle's parent: ${circle.parent}")

        // StackPane <- Pane <- Region <- Parent <- Node (implements EventTarget, Styleable)
        // Only Group (and WebView) extend Parent directly, other classes implicitly inherit
        // Parent via Control or Region.
        // Region is the base class for all JavaFX Node-based UI Controls, and all layout containers.
        // It is a resizable Parent node which can be styled from CSS. It can have multiple backgrounds
        // and borders.
//        val scene = Scene(StackPane(canvas, group, group)) // Error: duplicate children added
        val scene = Scene(StackPane(canvas, group))
        primaryStage.scene = scene
        primaryStage.title = "Basic JavaFx app"
        primaryStage.show()

        println("Circle's scene: ${circle.scene}")
        println("Circle's parent: ${circle.parent}")

        // If circle is not put into group:
//        Circle's scene: null
//        Circle's parent: null
//        Circle's scene: javafx.scene.Scene@23b429ba
//        Circle's parent: StackPane@6f5b409a[styleClass=root]

        // If it is:
//        Circle's scene: null
//        Circle's parent: Group@73416de4
//        Circle's scene: javafx.scene.Scene@5cd60b25
//        Circle's parent: Group@73416de4

        // Resizing the canvas when the stage is resized makes no noticeable
        // performance difference.
        primaryStage.widthProperty().addListener { observable, oldValue, newValue ->
            canvas.width = newValue.toDouble()
        }
        primaryStage.heightProperty().addListener { observable, oldValue, newValue ->
            canvas.height = newValue.toDouble()
        }
    }
}

fun main(args: Array<String>) {
    Application.launch(BasicApp::class.java, *args)
}