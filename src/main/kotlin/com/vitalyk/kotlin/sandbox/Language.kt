package com.vitalyk.kotlin.sandbox

import java.io.File
import java.util.*
import kotlin.properties.Delegates
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.memberProperties

// Kotlin compiler adds all top-level functions to respective auto-generated classes.
// If the filename of the file where our function resides is Main.kt, the class will
// be named 'MainKt'.
// To specify the class name manually,
// the following annotation can be used at the start of the file:
// @file:JvmName("MainCls")
fun main(args: Array<String>) {
//    val r = get(url = "https://api.github.com/events")
//    post("http://httpbin.org/post", data = mapOf("key" to "value"))
//    classes()
//    extensionFunctions()
//    delegates()

//    javaReflection()
    enums()
}

fun vararg(vararg params: String) {
    // vararg("hey", "hi", "hello")
    println(params.count())
    for (p in params) {
        println(p)
    }
}

interface Base {
    val message: String
    fun printMessage()
    fun printMessageLine()
}

class BaseImpl(val x: Int) : Base {
    override val message: String
        get() = "BaseImpl: x = $x"
    override fun printMessage() {
        print(x)
    }
    override fun printMessageLine() {
        println(x)
    }
}

class Derived(b: Base) : Base by b {
    override val message = "Message of Derived"
    override fun printMessage() {
        print("Hey")
    }
}

// https://kotlinlang.org/docs/reference/delegation.html
fun delegates() {
    val b = BaseImpl(10)
    Derived(b).printMessage() // Hey
    Derived(b).printMessageLine() // 10
    println(Derived(b).message) // Message of Derived

    class Delegate {
        // 'thisRef' is the instance that holds the 'property' we read
        operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
            return "$thisRef, thanks for delegating ${property.name} to me."
        }
        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
            println("$value has been assigned to ${property.name} in $thisRef")
        }
    }

    class Example {
        // a delegated property can also be declared inside a function or code block,
        // not just as a class member
        var p: String by Delegate()

        var name: String by Delegates.observable("<no name>") { prop, old, new ->
            println("$old -> $new")
        }

        var lastName: String = "<no last name>"
            set(value) {
                println("$field -> $value")
                field = value
            }
    }

    val ex = Example()
    val listener = { old: String, new: String -> println(old) }
    ex.p = "Vitaly"
    // Vitaly has been assigned to p in com.vitalyk.kotlin.sandbox.LanguageKt$delegates$Example@593634ad
    println(ex.p)
    // com.vitalyk.kotlin.sandbox.LanguageKt$delegates$Example@593634ad, thanks for delegating p to me.

    ex.name = "Vitaly"
    ex.lastName = "Kravchenko"
}

fun maps() {
    val m: MutableMap<String, Int> = mutableMapOf("Hello" to 1, "Hi" to 2)
    val a: Array<String> = arrayOf("A", "B", "C")
    var i = 3
    for (s in a) {
        m[s] = i++
    }

    println(m["A"])
    println(m["Hello"])
    println("A" in m)
    println("Hi" in m)

    val map = mutableMapOf<String, String>()
//    map["hey"] = null   // can't use null as a map value
    println(map["hello"]) // however, this is not an error, and simply returns null
    map.remove("hi")      // no such key, but not an error
}

data class RangeValue(
    val code: String,
    val name: String
)

enum class Range(val value: RangeValue) {
    Y5(RangeValue("5y", "5 Years")),
    Y2(RangeValue("2y", "2 Years")),
    Y(RangeValue("1y", "1 Year")),
    YTD(RangeValue("ytd", "YTD")),
    M6(RangeValue("6m", "6 Months")),
    M3(RangeValue("3m", "3 Months")),
    M(RangeValue("1m", "1 Month"));

    override fun toString(): String = value.name
}

fun enums() {
    // In Java and Kotlin enums are full blown classes.
    // The enum class body can include methods and other fields, not just enum constants.

    // The `Range` enum compiles to:

    // public final enum Range extends java/lang/Enum  {
    //
    //     public final static enum LRange; Y5
    //     public final static enum LRange; Y2
    //     public final static enum LRange; Y
    //     public final static enum LRange; YTD
    //     public final static enum LRange; M6
    //     public final static enum LRange; M3
    //     public final static enum LRange; M
    //
    //     private final LRangeValue; value
    //     public final getValue()LRangeValue;
    //
    //     public toString()Ljava/lang/String;

    // Each field of an enum is an instance of the enum class.
    // Each enum field that we create will be at least as much as the overhead
    // of creating an object in the JVM.

    println(Range.M6.value.name) // "6 Months"
    println(Range.M6)            // "6 Months" (toString)

    val enum1 = Range.Y
    val enum2 = Range.Y

    println(enum1 == enum2)
}

fun classes() {

    /**
     * On inheritance in data classes.
     *
     * Answer by Andrey Breslav on StackOverflow (Oct 14, 2014):
     *
     * The truth is: data classes do not play too well with inheritance.
     * We are considering prohibiting or severely restricting inheritance of data classes.
     * For example, it's known that there's no way to implement equals() correctly
     * in a hierarchy on non-abstract classes.
     * So, all I can offer: don't use inheritance with data classes.
     *
     * Since Kotlin 1.1, data classes may extend other classes (see Sealed classes).
     */

    // The primary constructor is a part of the class header.
    class Person constructor(firstName: String) {
    }
    // The constructor keyword can be omitted.
    class Person1(firstName: String) {

    }
    // The primary constructor cannot contain any code.
    // Initialization code can be placed in initializer blocks,
    // which are prefixed with the 'init' keyword.
    class Customer(name: String) {
        init {
            // Parameters of the primary constructor can be used
            // in the initializer blocks ...
            println("Customer initialized with the value $name.")
        }
        // ... they can also be used in property initializers declared
        // in the class body.
        val customerKey = name.toUpperCase()
    }
    val c = Customer("Germany")
    println(c.customerKey)

    // Declaring properties and initializing them from the primary
    // constructor.
    class Customer1(val name: String, var age: Int)
    val c1 = Customer1("Vitaly", 31)
    c1.age = 20
    println("${c1.name} is ${c1.age} years old.")

    class Customer2 {
        var name = ""
        constructor(parent: Customer1) {
            name = "--- " + parent.name + " ---"
            println(name)
        }
    }
    val c2 = Customer2(c1)
}

// Interfaces are different from abstract classes because they cannot store state.
// Properties declared in interfaces can't have backing fields,
// and therefore accessors declared in interfaces can't reference them.
interface MyInterface {
    var prop: Int // abstract
    val propertyWithImplementation: String
        get() = "baz" // can't use `field` here
    fun bar()
    fun foo() {
        // optional body
    }
}

interface A {
    fun foo() { print("A") }
    fun bar() // if an interface function has no body, it is marked abstract automatically
}

interface B {
    fun foo() { print("B") }
    fun bar() { print("bar") }
}

fun interfaces() {
    class Child : MyInterface {
        override var prop = 5
            get() = field
            set(value) { field = value }
        override fun bar() {
            println("bar")
        }
        override fun foo() {
            println("foo")
        }
    }

    class C : A {
        override fun bar() { print("bar") }
    }

    class D : A, B {
        override fun foo() {
            super<A>.foo()
            super<B>.foo()
        }

        override fun bar() {
            super<B>.bar()
        }
    }
}

interface MySamListener {
    fun call(new: String, old: String)
}

fun samConversion() {
    // SAM conversion (automatic conversion of a lambda into an interface
    // is only supported for interfaces defined in Java code).

    fun addListener(listener: MySamListener) {
        // ...
    }

    // Doesn't work.
    // addListener { new, old -> println("$new, $old") }

    // Works.
    addListener(object : MySamListener {
        override fun call(new: String, old: String) {
            println("$new, $old")
        }
    })
}

fun javaReflection() {
    data class Person(
        val name: String?,
        var age: Int,

        val bool: Boolean,
        val int: Int,
        val long: Long,
        val float: Float,
        val double: Double,
        val date: Date
    )

    println(Person::class.java) // class com.vitalyk.kotlin.sandbox.LanguageKt$javaReflection$Person
    println(Person::javaClass)  // val T.javaClass: java.lang.Class<T>

    println(Person::class.java.constructors.first().parameters.first().name)
    println(Person::class.java.constructors.first().parameters.first().type.simpleName)

    println("--- Java's reflection ---")
    Person::class.java.declaredMethods.forEach {
        println("${it.name}: ${it.returnType}")
        println("${it.returnType.simpleName} from ${it.returnType.`package`}")
    }

    println("--- Kotlin's reflection ---")
    Person::class.memberProperties.forEach {
        println("${ if (it is KMutableProperty<*>) "var" else "val" } ${it.name}: ${it.returnType}")
    }
}

fun reflection() {
    // - Class references.
    class MyClass
    // class literal syntax
    val c = MyClass::class // get the reference to a Kotlin class
    // the reference is of type KClass
    println(c)
    println(c.java) // Java class reference

    // - Function references.
    fun isOdd(x: Int) = x % 2 != 0
    fun isOdd(s: String) = s == "bob" || s == "tom" || s == "sam"
    val numbers = listOf(1, 2, 3)
    // This works with overloaded functions when the expected type is known from the context.
    println(numbers.filter(::isOdd)) // pass function as a value to another function
    // Here ::isOdd is a value of function type (Int) -> Boolean.

    val predicate: (String) -> Boolean = ::isOdd // refers to isOdd(u: String)
    // If we need to use a member of a class, or an extension function, it needs to be
    // qualified.

    fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C {
        return { x -> f(g(x)) }
    }
    fun length(s: String) = s.length
    val oddLength = compose(::isOdd, ::length)
    val strings = listOf("a", "ab", "abc")
    println(strings.filter(oddLength))

}

fun loops() {
    val collection = listOf<Any>()
    for (item in collection) print(item)

    val ints = intArrayOf()
    for (item: Int in ints) { }

    for (i in 1..3) { println(i) }

    for (i in 0 until 10) { }

    for (i in 6 downTo 0 step 2) { }
}

fun returns() {
    loop1@ for (i in 1..3) {
        for (j in 1..3) {
            println("$i $j")
            if (i == 2 && j == 2) {
                break@loop1 // jumps to the execution point right after the loop marked with the label
            }
        }
    }

    //    1 1
    //    1 2
    //    1 3
    //    2 1
    //    2 2

    println("--------")

    loop2@ for (i in 1..3) {
        for (j in 1..3) {
            println("$i $j")
            if (i == 2 && j == 2) {
                continue@loop2 // proceeds to the next iteration of the loop
            }
        }
    }

    //    1 1
    //    1 2
    //    1 3
    //    2 1
    //    2 2
    //    3 1
    //    3 2
    //    3 3

    println("--------")

    val ints = arrayOf(1, 2, 3)

    (fun () {
        ints.forEach {
            if (it == 2) return  // Returns from the outer function.
            // Note: such non-local returns are supported only for lambda experessions
            // passed to inline functions.
            print(it)
        }
        print("!")  // This never runs.
    })() // prints: 1

    println("")

    // If we need to return from a lambda expression, we need to label it and qualify
    // the return.
    (fun () {
        ints.forEach {
            if (it == 2) return@forEach
            print(it)
        }
        print("!")
    })() // prints: 13!

    println("")

    // Alternatively, we can replace the lambda expression with an anonymous function.
    (fun () {
        ints.forEach(fun (value: Int) {
            if (value == 2) return
            print(value)
        })
        print("!") // prints: 13!
    })()

    println("")

    // When returning a value, the parser gives preference to the qualified return.
    // Below code means "return 1 at label @a" and not "return a labeled expression (@a 1)".
    a@ fun foo(): Int {
        return@a 1
    }
    foo()
}

//fun inline() {
//    // https://kotlinlang.org/docs/reference/inline-functions.html
//}

fun sorting() {

    data class Person(
            val name: String,
            val age: Int
    )

    val objects: Array<Person> = arrayOf(
            Person("Bob", 42),
            Person("Mary", 35),
            Person("Mike", 14),
            Person("Bryan", 8)
    )

    println(objects.toList())
    objects.sortWith(compareBy<Person> { it.age } .thenBy<Person> { it.name })
    println(objects.toList())
}

fun filtering() {
    val integers: Array<Int> = arrayOf(1, 2, 3, 4, 5, 6, 7)
    val even = integers.filter { x -> x % 2 == 0 }
    println(even)

    println(arrayOf(1, 2, 3, 4, 5, 6, 7).filter { x -> x % 2 == 1 })
    // [1, 2, 3, 4, 5, 6, 7].filter(u => u % 2 == 1)  // same in JavaScript

    println(Array(5, { i -> (i * i).toString() }).toList())

    println(arrayOf(-1, 5, -3, 7, 9, -8).filter { it > 0 })

//    collection.filter { it.isActive() }
//            .map { it.size() }
//            .reduce { it, sum -> it + sum }
}

fun traversing() {
    val map = mapOf(
            "Vitaly" to "Kravchenko",
            "Natasha" to "Zhelezniy",
            "Lena" to "Bagno"
    )

    for ((k, v) in map) {
        println("$k -> $v")
    }
}

fun ranges() {
//    for (i in 0..3) { // closed range, [0..3]
//        println("i = ${i}")
//    }
//    for (i in 0 until 3) { // open range, [0..3)
//        println("i = ${i}")
//    }
//    for (i in 2..10 step 2) {
//        println("i = ${i}")
//    }
//    for (i in 3 downTo 0) { // closed range, [3..0]
//        println("i = ${i}")
//    }
//    for (i in 4 downTo 1 step 2) print(i)

    val a = 1..10
    val b = 6..20

    println(a + b)  // (1..10) + (6..20)

    val x = 1..10
    val y = 13..20

    println(x + y)  // (1..10) + (13..20)

    println((3..3) + (7..7))

    println(y.start) // 13
    println(y.endInclusive)  // 20
    println(y.endInclusive - y.start + 1)  // 8
    println(y.count())  // 8

    val z = y.toList()
    println(z)  // [13, 14, 15, 16, 17, 18, 19, 20]
    // below makes a slice from 15th to 19th item
//    println(v.toList().slice(15..18))  // IndexOutOfBoundsException
    println(z.slice(0..7))  // [13, 14, 15, 16, 17, 18, 19, 20]
    println(z.take(z.size - 1))
}

fun checks() {
    class Foo {
        fun poo() {
            println("To poo or not to poo. That is the question.")

            when (Math.random() * 9) {
                in 0..3 -> println("OK, I just pooped. Enjoy.")
                in 3..6 -> println("Maybe next time.")
                in 6..9 -> println("Still thinking.")
            }
        }
    }
    class Bar {
        fun put() {
            println("I just put.")
        }
    }

    val x: Any = Foo()

    when (x) {
        is Foo -> x.poo()
        is Bar -> x.put()
        else -> println(x)
    }
}

fun casts() {
    var obj = Any()

    if (obj is String) {
        println(obj.length) // automatic type cast to String because we checked
    }
    if (obj !is String) {
        println("Not a String.")
    }

    // "Unsafe" cast operator
    val a: Any = "Hey"
    val x: String = a as String
//    val v: String = null as String // throws
    val y: String? = null as String? // correct way

    println(x)
    println(y)

    // "Safe" (nullable) cast operator
    val b: Any = 5
    var z: String? = b as? String

    println(z) // prints "null" as r is not a String
}

fun bitwise() {
    println(1 shl 2)
    println(1.shl(2))
    println(-2 shr 3)
    println(-2 ushr 3)
    println(1 and 2)
    println(1 or 2)
    println(1 xor 2)
    println(1.inv())
}

fun arrays() {
    // Array vs List:
    // * Array - single implementation, List - many subclasses
    // * Array - mutable, List - not
    // * Array - fixed size, e.g. MutableList has 'add' and 'remove' methods
    // * Array<T> - invariant on T, List - covariant
    // * Arrays are optimized for primitives (IntArray, DoubleArray, etc.),
    //   List has no implementations optimized for primitives
    // * Array and List have different mapping rules for Java interoperability
    //   https://kotlinlang.org/docs/reference/java-interop.html#mapped-types
    //   https://kotlinlang.org/docs/reference/java-interop.html#java-arrays
    // * Using Lists over Arrays is preferred for everything, except for performance
    //   critical parts: http://www.javapractices.com/topic/TopicAction.do?Id=39
}

fun strings() {
    val greeting = "Hello"

    for (c in greeting) {
        print(c + " ")
    }
    println()

    var text = """
        for (g in greeting) {
            print(g + " ")
        }
    """
    println(text)
    text = """
        |for (g in greeting) {
        |    print(g + " ")
        |}
    """
    println(text)
    println(text.trimMargin())

    val c = 'a' // Char
    if (c !in '0'..'9') {
        println("'a' is not a digit.")
    }
}

fun functions() {
    // https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/apply.html
//    compareValuesBy(lhs, rhs,
//            {it.active}, {it.lastName}, {it.firstName})

    fun default(value: Int?): Int {
        val defaultValue = 5
        return value ?: defaultValue
    }
    println(default(10))
    println(default(null))

    fun theAnswer() = 42 // single-expression functions
    println(theAnswer())

    // Builder-style usage of methods that return Unit
    fun arrayOfMinusOnes(size: Int): IntArray {
        return IntArray(size).apply { fill(-1) }
    }

    // x is a function of type: () -> Unit
    val x = {
        println("hey")
    }
    // y is a function of type: () -> Int
    val y = {
        1 + 3
    }
    // z is a function of type: (Int, Int) -> String
    val z = { n1: Int, n2: Int ->
        (n1 + n2).toString()
    }
    // the type of fullZ is explicitly defined, no need to define the types of n1, n2
    val fullZ: (Int, Int) -> String = { n1, n2 ->
        (n1 + n2).toString()
    }
}

fun laziness() {
    val field by lazy { "I'm lazy." }  // Requires Kotlin 1.1

//    println(field)
}

fun extensionFunctions() {
    // https://kotlinlang.org/docs/reference/extensions.html

    // Extensions add the ability to extend a class with new functionality without having
    // to inherit from the class or use any type of design pattern such as Decorator.

    // To declare an extension function, we need to prefix its name with a receiver type,
    // i.e. the type being extended.

    fun String.spaceToCamelCase(): String {
        val parts = this.split(" ");
        val caps = parts.map(String::capitalize)
        // The above is equivalent to
        // val caps = parts.map {
        //     it -> it.capitalize()
        // }
        return caps.joinToString(" ")
    }

    println("Convert this to camelcase".spaceToCamelCase())

    // The following adds a swap function to MutableList<Int>:
//    fun MutableList<Int>.swap(index1: Int, index2: Int) {
//        val tmp = this[index1] // 'this' corresponds to the list
//        this[index1] = this[index2]
//        this[index2] = tmp
//    }
    // Make it generic
    fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
        val tmp = this[index1] // 'this' corresponds to the list
        this[index1] = this[index2]
        this[index2] = tmp
        // The this keyword inside an extension function corresponds to the receiver object.
    }

    val l = mutableListOf(1, 2, 3)
    l.swap(0, 2)
    println(l)
}

fun stringTemplates() {

    data class Person(
            val name: String,
            val age: Int
    )

    val objects: Array<Person> = arrayOf(
            Person("Bob", 42),
            Person("Mary", 35)
    )

    println("This is a string template. ${objects[0].name} is ${objects[0].age} years old.")
}

object Resource { // cannot be local
    val name = "Name"
}

fun singletons() {
    println(Resource.name)
}

fun shorthands() {
    val files = File(".").listFiles()

    println(files?.size) // if not null
    println(files?.size ?: "empty") // if not null else

    for (f in files) {
        println(f)
    }

    val data = mapOf("name" to "Vitaly")
    // execute if null
//    val email = data["email"] ?: throw IllegalStateException("Email is missing!")

    var n: Any? = null
    n?.let {
        // execute this block if not null
        println(n)
    }

    fun transform(color: String): Int {
        return when (color) {
            "Red" -> 0
            "Green" -> 1
            "Blue" -> 2
            else -> throw IllegalArgumentException("Invalid color param value")
        }
    }
    println(transform("Green"))
    println(transform("Yellow"))

//    fun sandbox() {
//        val result = try {
//            count()
//        } catch (e: ArithmeticException) {
//            throw IllegalStateException(e)
//        }
//
//        // Working with result
//    }
}

fun conditionals() {
    fun max(a: Int, b: Int): Int {
        if (a > b) {
            return a
        } else {
            return b
        }
    }

    fun min(a: Int, b: Int) = if (a < b) a else b

    println("${max(5, 8)} ${min(6, 3)}")

    fun foo(param: Int) {
        val result = if (param == 1) {
            "one"
        } else if (param == 2) {
            "two"
        } else {
            "three"
        }
    }
}

fun nullables() {
    fun parseIntNoNull(str: String): Int {
        return str.length // "return null" won't work here
    }

    // return null if str doesn't hold an integer
    fun parseInt(str: String): Int? {
        return null
    }

    println(parseInt("10"))
}

fun generics() {
    class Box<T>(t: T) {
        var value = t
    }
    val box = Box<Int>(1)
    println(box.value)

    /*

    One of the most tricky parts of Java's type system is wildcard types.
    And Kotlin doesn't have any.
    Instead, it has two other things: declaration-site variance and type projections.

    Generic types in Java are invariant,
    meaning that List<String> is not a subtype of List<Object>.
    If it was, this would have been possible:

    // Java
    List<String> strs = new ArrayList<String>();
    List<Object> objs = strs; // !!! The cause of the upcoming problem sits here. Java prohibits this!
    objs.add(1); // Here we put an Integer into a list of Strings
    String s = strs.get(0); // !!! ClassCastException: Cannot cast Integer to String

    // Java
    interface Collection<E> ... {
        void addAll(Collection<E> items);
    }
    void copyAll(Collection<Object> to, Collection<String> from) {
        to.addAll(from); // !!! Would not compile with the naive declaration of addAll:
                         // Collection<String> is not a subtype of Collection<Object>
    }

    That's why the actual signature of addAll() is the following:
    interface Collection<E> ... {
        void addAll(Collection<? extends E> items);
    }

    The wildcard type argument <? extends E> indicates that this method accepts
    a collection of objects of E or some subtype of E, not just E itself.
    This means that we can safely read E's from items (elements of this collection
    are instances of a subclass of E), but cannot write to it since we do not know what
    objects comply to that unknown subtype of E.
    In return for this limitation, we have the desired behaviour:
    Collection<String> is a subtype of Collection<? extends Object>.
    In "clever words", the wildcard with an extends-bound (upper bound) makes the type covariant.

    Collection<? extends Object> // Covariant type, something that extends Object.
    Can read Strings from the collection, but can only write Objects.

    List<? super String> // Contravariant type, something that has String extends.
    Can only read Objects from the collection, but only write Strings.

    Collection<String> is a subtype of Collection<? extends Object>
    List<? super String> a supertype of List<Object>

    Producer-Extends, Consumer-Super
    Can only read from Producers, can only write to Consumers.

    */

    abstract class Source<out T> {
        abstract fun nextT(): T
    }

    fun demo(strs: Source<String>) {
        val objects: Source<Any> = strs
    }

    // The general rule is: when a type parameter T of a class C is declared out,
    // it may occur only in out-position in the members of C.

    abstract class Comparable<in T> {
        abstract fun compareTo(other: T): Int
    }

    fun demo(x: Comparable<Number>) {
        x.compareTo(1.0) // 1.0 has type Double, which is a subtype of Number
        // Thus, we can assign u to a variable of type Comparable<Double>
        val y: Comparable<Double> = x // OK!
    }

    // Consumer in, Producer out.
}

fun collections() {
    // https://kotlinlang.org/docs/reference/collections.html

    val numbers = mutableListOf<Int>(1, 2, 3) // returns MutableList<Int>
    val readOnlyView: List<Int> = numbers

    val realOnlyList = listOf(1, 2, 3)

    println(numbers)
    numbers.add(4)
    println(readOnlyView)

    val strings = hashSetOf("a", "r", "g", "g")
    assert(strings.size == 3)
    println(strings)

    // Inferred type: Map<String, Int>
    val readOnlyMap = mapOf("a" to 1, "r" to 2, "g" to 3)
    println(readOnlyMap)

    val map = mutableMapOf("a" to 1, "r" to 2)
    map["g"] = 3
    println(map["g"])
}