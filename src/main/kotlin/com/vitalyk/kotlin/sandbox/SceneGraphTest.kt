package com.vitalyk.kotlin.sandbox

import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.scene.shape.Arc
import javafx.scene.shape.ArcType
import javafx.stage.Stage

class SceneGraphTest: Application() {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(SceneGraphTest::class.java, *args)
        }
    }

    override fun start(primaryStage: Stage) {
        val width = 800.0
        val height = 400.0

        val group = Group()
        val scene = Scene(group, width, height)

        val range = 1..5000
        val arcs = range.map {
            Arc().apply {
                centerX = 0.0
                centerY = 0.0
                radiusX = 7.0
                radiusY = 7.0
                startAngle = 0.0
                length = 270.0
                fill = Color.RED
                stroke = Color.BLACK
                strokeWidth = 3.0
                type = ArcType.CHORD
                translateX = Math.random() * width
                translateY = Math.random() * height
            }
        }
        val deltas = range.map {
            arrayOf(Math.random() - 0.5, Math.random() - 0.5)
        }

        group.children.addAll(arcs)

        primaryStage.scene = scene
        primaryStage.title = "Canvas Test"
        primaryStage.show()

        var fps = 0
        var start = System.nanoTime()

        object : AnimationTimer() {
            override fun handle(now: Long) {
                arcs.forEachIndexed { i, arc ->
                    val delta = deltas[i]

                    arc.translateX += delta[0]
                    arc.translateY += delta[1]

                    if (arc.translateX > width) {
                        arc.translateX -= width
                    }
                    else if (arc.translateX < 0) {
                        arc.translateX += width
                    }

                    if (arc.translateY > height) {
                        arc.translateY -= height
                    }
                    else if (arc.translateY < 0) {
                        arc.translateY += height
                    }
                }

                fps++
                if ((now - start) / 1_000_000 > 1000) {
                    primaryStage.title = "FPS: $fps"
                    fps = 0
                    start = now
                }
            }
        }.start()
    }
}