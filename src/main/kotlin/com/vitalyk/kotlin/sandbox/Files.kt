package com.vitalyk.kotlin.sandbox

import java.io.*

fun main(args: Array<String>) {
//    kotlin()
}

fun simple() {
    var input: InputStream? = null
    var output: OutputStream? = null
    try {
        input = FileInputStream("input.txt")
        output = FileOutputStream("output.txt")
        var byte: Int
        var hasInput: Boolean = true
        while (hasInput) {
            byte = input.read()
            hasInput = byte >= 0
            if (hasInput) {
                output.write(byte)
            }
        }
    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    } catch (e: IOException) {
        e.printStackTrace()
    } finally {
        input?.close()
        output?.close()
    }
}

fun kotlin() {
    // http://stackoverflow.com/questions/35444264/how-do-i-write-to-a-file-in-kotlin
    File("kotlin.txt").printWriter().use { out ->
        out.println("Hello, Kotlin.")
    }
}