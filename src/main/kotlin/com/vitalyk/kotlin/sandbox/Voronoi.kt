package com.vitalyk.kotlin.sandbox

import javafx.animation.AnimationTimer
import javafx.application.Application
import javafx.geometry.Point2D
import javafx.scene.Group
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.Arc
import javafx.scene.shape.Circle
import javafx.scene.shape.Line
import javafx.scene.text.Text
import tornadofx.*

class VoronoiView: View() {
    override val root = Pane()

    val points = mutableListOf<Point2D>()
    val width = 800.0
    val height = 800.0
    val count = 50

    init {
        primaryStage.minWidth = width
        primaryStage.minHeight = height

        val group = Group()
        root.add(group)

        for (i in 0 until count) {
            val point = Point2D(Math.random() * width, Math.random() * height)
            points.add(point)
            group.add(Circle(point.x, point.y, 3.0))
            val arc = Arc(point.x, point.y, 10.0, 20.0, 0.0, 180.0)
            arc.fill = null
            arc.stroke = Color.RED
            group.add(arc)
        }

        val line = Line(0.0, 0.0, 0.0, height)
        line.stroke = Color.RED
        group.add(line)

        println(line.boundsInLocal)

        val text = Text("Hello world")
        text.x = 200.0
        text.y = 200.0
        group.add(text)

        println(text.boundsInLocal)

//        line.id = "hello"
//        val selection = group.lookup("#hello")
//        if (selection is Line) {
//            println(selection)
//        }

        val selection = group.lookupAll("Arc")
        println(selection.size)

        object: AnimationTimer() {
            override fun handle(now: Long) {
                line.startX++
                line.endX++
            }
        }.start()
    }
}

class VoronoiApp: App(VoronoiView::class)

fun main(args: Array<String>) {
    Application.launch(VoronoiApp::class.java, *args)
}