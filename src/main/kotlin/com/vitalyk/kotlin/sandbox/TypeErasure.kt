package com.vitalyk.kotlin.sandbox

import java.util.*
import kotlin.reflect.KClass

// Error: declaration have the same JVM signature
//class MyClass {
//    fun print(list: List<String>) {}
//    fun print(list: List<Int>) {}
//}

class Dude {
    val nick: String = ""
}

fun main(args: Array<String>) {
    val list1 = listOf<Int>()
    val list2 = listOf<Float>()

    if (list1::class.java === list2::class.java) {
        println("Equal")
    }
}

// "Type parameter T cannot be called as a function" because T is erased.
//fun <T> initElementType(arg: List<T>) = T()

// Works fine.
fun initDude(arg: List<Dude>): Dude = Dude()

fun typeErasure() {
    // Before Java 5 (2004)

    //    List objects = new ArrayList();
    //    objects.add("One");
    //    objects.add("Two");
    //    objects.add("Three");
    //    Object object = objects.get(0);
    //    String string = (String) object; // Explicit casting required

    // Java 5+

    //    List<String> strings = new ArrayList<String>();
    //    strings.add("One");
    //    strings.add("Two");
    //    strings.add("Three");
    //    String string = strings.get(0); // No more casting!

    // There have been many changes in the syntax and API throughout Java history.
    // One of the most important guidelines regarding those changes is backward compatibility.

    // Regarding generics, this means that parameterized types are not stored in the bytecode.
    // This is called type erasure, because parameterized types are “erased”.
    // Generics are enforced at compile-time by the compiler itself.

    // For example:

    //    List objects = new ArrayList();
    //    List<String> strings = new ArrayList<String>();
    //    List<Long> longs = new ArrayList<Long>();

    // Byte-code:

    //    L0
    //    LINENUMBER 9 L0
    //    NEW java/util/ArrayList
    //    DUP
    //    INVOKESPECIAL java/util/ArrayList.<init> ()V
    //    ASTORE 1
    //    L1
    //    LINENUMBER 10 L1
    //    NEW java/util/ArrayList
    //    DUP
    //    INVOKESPECIAL java/util/ArrayList.<init> ()V
    //    ASTORE 2
    //    L2
    //    LINENUMBER 11 L2
    //    NEW java/util/ArrayList
    //    DUP
    //    INVOKESPECIAL java/util/ArrayList.<init> ()V
    //    ASTORE 3

    // Let check in Kotlin:

    val objects = listOf<Any>()
    val strings = listOf<String>()
    val longs = listOf<Long>()

    // Byte-code:

    //    public final static typeErasure()V
    //    L0
    //    LINENUMBER 1000 L0
    //    L1
    //    INVOKESTATIC kotlin/collections/CollectionsKt.emptyList ()Ljava/util/List;
    //    L2
    //    LINENUMBER 1000 L2
    //    ASTORE 0
    //    L3
    //    LINENUMBER 1001 L3
    //    L4
    //    INVOKESTATIC kotlin/collections/CollectionsKt.emptyList ()Ljava/util/List;
    //    L5
    //    LINENUMBER 1001 L5
    //    ASTORE 1
    //    L6
    //    LINENUMBER 1002 L6
    //    L7
    //    INVOKESTATIC kotlin/collections/CollectionsKt.emptyList ()Ljava/util/List;
    //    L8
    //    LINENUMBER 1002 L8
    //    ASTORE 2
    //    L9
    //    LINENUMBER 1003 L9
    //    RETURN
    //    L10
    //    LOCALVARIABLE longs Ljava/util/List; L9 L10 2
    //    LOCALVARIABLE strings Ljava/util/List; L6 L10 1
    //    LOCALVARIABLE objects Ljava/util/List; L3 L10 0
    //    MAXSTACK = 1
    //    MAXLOCALS = 3

    // Issues:

    // - Method names

    // Since generics are not written in the bytecode, they do not affect the signature
    // of a method. Hence, methods that have the same name and the same arguments –
    // stripped of generics, have the same signature.

    //    class Invalid { // won't compile
    //        // Declarations have the same JVM signature: invalid(Ljava/util/List;)V
    //        fun invalid(strings: List<String>) {}
    //        fun invalid(objects: List<Any>) {}
    //    }

    // - Reflection

    // As generics are not stored in the bytecode, there’s no way to get parameterized
    // types using reflection.


    // Solutions:

    // - Changing the method name (won't solve reflection issue)
    // - Passing an extra Class parameter (to check what it is and act based on that)

    class GenericTrick {

        fun <T: Any> withClass(list: List<T>, clazz: KClass<T>) {
            when (clazz) {
                Int::class -> { }
                Date::class -> { }
                else -> { }
            }
        }
    }

    // This way, the compiler enforces that the collection and the class both have
    // the same parameterized type. The class type is written in the bytecode,
    // and thus it can be obtained using reflection.

    // However, Kotlin offers a great way to do without the extra parameter.
    // It means T can be used as it is. This is achieved by using the `reified` keyword,
    // while declaring T.
    //
    // There’s a caveat though: reified generics can only be used when functions are inlined.

    class GenericTrick2 {

        inline fun <reified T: Any> withClass(list: List<T>) {
            when (T::class) {
                Int::class -> { }
                Date::class -> { }
                else -> { }
            }
        }
    }
}