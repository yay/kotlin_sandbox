package com.vitalyk.kotlin.sandbox

import javafx.application.Application
import javafx.beans.property.SimpleDoubleProperty
import javafx.collections.FXCollections
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.HBox
import javafx.stage.Stage
import javafx.util.Callback
import tornadofx.*

class Foo {
    val barProperty = SimpleDoubleProperty()
    fun barProperty() = barProperty
    var bar by barProperty

    var baz by property<Double>()
    fun bazProperty() = getProperty(Foo::baz)
}

class FooApp: Application() {
    override fun start(primaryStage: Stage) {
        val foos = FXCollections.observableArrayList(
            Foo().apply { bar = 42.0 }
        )

        val table = TableView<Foo>(foos)

        val barColumn = TableColumn<Foo, Double>("Bar").apply {
            prefWidth = 100.0
        }
        barColumn.cellValueFactory = PropertyValueFactory<Foo, Double>("bar")
        barColumn.setCellFactory {
            FooTableCell<Foo, Double> { "%.2f".format(it) }
        }

        val changeColumn = TableColumn<Foo, Unit>()
        changeColumn.cellFactory = Callback {
            object : TableCell<Foo, Unit>() {
                private val incButton = Button("+").apply {
                    setOnAction {
                        (tableRow.item as Foo).bar++
                    }
                }
                private val decButton = Button("-").apply {
                    setOnAction {
                        (tableRow.item as Foo).bar--
                    }
                }
                private val g = HBox(2.0, incButton, decButton)

                override fun updateItem(item: Unit?, empty: Boolean) {
                    super.updateItem(item, empty)
                    graphic = if (empty) null else g
                }
            }
        }

        table.columns.addAll(barColumn, changeColumn)

        val scene = Scene(table, 400.0, 200.0)
        primaryStage.scene = scene
        primaryStage.title = "Table Cell"
        primaryStage.show()

//        launch {
//            while (isActive) {
//                delay(500)
//                Platform.runLater {
//                    val oldFoo = foos[0]
//                    // Replacing the old Foo instance with a new one,
//                    // updating the value of the `bar` field:
//                    foos[0] = Foo().apply {
//                        bar = oldFoo.bar - 1.0 + Math.random() * 2.0
//                    }
//                }
//                // because a change to a field cannot be detected by an observable list
//                // and so does not propagates to the table. This won't result in
//                // a visible change:
//                // foos[0].bar = foos[0].bar - 1.0 + Math.random() * 2.0
//            }
//        }
    }
}

class FooTableCell<S, T>(private val format: (T) -> String) : TableCell<S, T>() {

    init {
        contentDisplay = ContentDisplay.GRAPHIC_ONLY

        itemProperty().addListener(ChangeListener { obs, oldItem, newItem ->
            if (newItem != null && oldItem != null && newItem != oldItem) {
                // This is never true.
                println("!!! Old: $oldItem, New: $newItem")
            } else {
                println("Change listener:\nOld: $oldItem, New: $newItem\n")
            }
        })
    }

    override fun updateItem(item: T?, empty: Boolean) {
        val oldItem = this.item
        super.updateItem(item, empty)

        if (item != null && oldItem != null && item != oldItem) {
            // This is never true.
            println("!!! Old: $oldItem, New: $item")
        } else {
            println("updateItem:\nOld: $oldItem, New: $item\n")
        }

        if (empty || item == null) {
            graphic = null
            text = null
        } else if (tableRow != null) {
            val cell = this
            graphic = Label().apply {
                textProperty().bindBidirectional(cell.textProperty())
            }
            text = format(item)
        }
    }
}

fun main(args: Array<String>) {
    Application.launch(FooApp::class.java, *args)
}