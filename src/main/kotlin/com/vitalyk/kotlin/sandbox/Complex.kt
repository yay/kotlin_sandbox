package com.vitalyk.kotlin.sandbox

/*

  https://www.khanacademy.org/math/algebra-home/alg-complex-numbers

  Real: 0, 1, 0.3, π, e

  In the real number system, there is no solution for u * u = -1.

  i * i = -1
  i = sqrt(-1)

  Imaginary: i, -i, πi, ei

  Complex: z = 5 + 3i

  Re(z) = 5  // real part
  Im(z) = 3  // imaginary part

  Every pure imaginary number is also a complex number, e.g. 0 + 9i.
  Every pure real number is also a complex number, e.g. 2 + 0i.

  Im
  ^
  |  Complex Plane
  |
3 |          z
  |
  |
--|------------> Re
  |          5

  Applications: electrical engineering, quantum mechanics,
                allow to solve any polynomial equation, ...

  2(2 + 3i) = 4 + 6i
  2i(2 - 3i) = 4i - 6i^2 = 6 + 4i
  (1 + 4i)(5 + i) = 5 + i + 20i + 4i^2 = 5 - 4 + 21i = 1 + 21i

*/

data class Complex(val real: Double, val imaginary: Double) {
    companion object {
        val zero = Complex(0.0, 0.0)
    }

    fun reciprocal(): Complex {
        val scale = (real * real) + (imaginary * imaginary)
        return Complex(real / scale, -imaginary / scale)
    }

    fun abs(): Double = Math.hypot(real, imaginary)

    operator fun unaryMinus(): Complex = Complex(-real, -imaginary)
    operator fun plus(other: Double): Complex = Complex(real + other, imaginary)
    operator fun minus(other: Double): Complex = Complex(real - other, imaginary)
    operator fun times(other: Double): Complex = Complex(real * other, imaginary * other)
    operator fun div(other: Double): Complex = Complex(real / other, imaginary / other)

    operator fun plus(other: Complex): Complex =
        Complex(real + other.real, imaginary + other.imaginary)

    operator fun minus(other: Complex): Complex =
        Complex(real - other.real, imaginary - other.imaginary)

    operator fun times(other: Complex): Complex =
        Complex(
            (real * other.real) - (imaginary * other.imaginary),
            (real * other.imaginary) + (imaginary * other.real)
        )

    operator fun div(other: Complex): Complex = this * other.reciprocal()

}