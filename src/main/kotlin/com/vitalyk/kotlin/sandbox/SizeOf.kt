package com.vitalyk.kotlin.sandbox

fun main(args: Array<String>) {
    println("A  Byte   variable occupies:  ${java.lang.Byte.SIZE / 8} byte")        // 1
    println("A  Short  variable occupies:  ${java.lang.Short.SIZE / 8} bytes")      // 2
    println("An Int    variable occupies:  ${java.lang.Integer.SIZE / 8} bytes")    // 4
    println("A  Long   variable occupies:  ${java.lang.Long.SIZE / 8} bytes")       // 8

    println("A  Float  variable occupies:  ${java.lang.Float.SIZE / 8} bytes")      // 4
    println("A  Double variable occupies:  ${java.lang.Double.SIZE / 8} bytes")     // 8

    println("A  Char   variable occupies:  ${java.lang.Character.SIZE / 8} bytes")  // 2
}

/*

Generally, size of any Java object in memory is a sum of following:

- object header
- memory for primitive fields
- memory for reference fields
- padding, usually multiple by 8 byte

On 64-bit JVM object header has size 16 bytes and following form:

|------------------------------------------------------------------------------------------------------------|--------------------|
|                                            Object Header (128 bits)                                        |        State       |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
|                                  Mark Word (64 bits)                         |    Klass Word (64 bits)     |                    |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
| unused:25 | identity_hashcode:31 | unused:1 | age:4 | biased_lock:1 | lock:2 |    OOP to metadata object   |       Normal       |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
| thread:54 |       epoch:2        | unused:1 | age:4 | biased_lock:1 | lock:2 |    OOP to metadata object   |       Biased       |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
|                       ptr_to_lock_record:62                         | lock:2 |    OOP to metadata object   | Lightweight Locked |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
|                     ptr_to_heavyweight_monitor:62                   | lock:2 |    OOP to metadata object   | Heavyweight Locked |
|------------------------------------------------------------------------------|-----------------------------|--------------------|
|                                                                     | lock:2 |    OOP to metadata object   |    Marked for GC   |
|------------------------------------------------------------------------------|-----------------------------|--------------------|

For example:

class Person {
    String name;
    int age;
    boolean male;
}

16 bytes (object header) + 8 bytes (reference to String variable name) +
4 bytes (int) + 1 byte (boolean) + 3 bytes (for padding) = 32 bytes

*/